package com.theyestech.yestech_mobile_app.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.theyestech.yestech_mobile_app.models.Category;
import com.theyestech.yestech_mobile_app.models.QuestionEnumeration;
import com.theyestech.yestech_mobile_app.models.QuestionMultipleChoice;
import com.theyestech.yestech_mobile_app.models.QuestionTrueFalse;

import java.util.ArrayList;
import java.util.List;

public class QuizDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "MyDummyQuiz.db";
    private static final int DATABASE_VERSION = 1;

    private static QuizDbHelper instance;

    private SQLiteDatabase db;

    private QuizDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized QuizDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new QuizDbHelper(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        final String SQL_CREATE_CATEGORIES_TABLE = "CREATE TABLE " +
                QuizContract.CategoriesTable.TABLE_NAME + "( " +
                QuizContract.CategoriesTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuizContract.CategoriesTable.COLUMN_NAME + " TEXT " +
                ")";

        final String SQL_CREATE_QUESTIONS_TABLE = "CREATE TABLE " +
                QuizContract.QuestionsTable.TABLE_NAME + " ( " +
                QuizContract.QuestionsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuizContract.QuestionsTable.COLUMN_QUESTION + " TEXT, " +
                QuizContract.QuestionsTable.COLUMN_OPTION1 + " TEXT, " +
                QuizContract.QuestionsTable.COLUMN_OPTION2 + " TEXT, " +
                QuizContract.QuestionsTable.COLUMN_OPTION3 + " TEXT, " +
                QuizContract.QuestionsTable.COLUMN_OPTION4 + " TEXT, " +
                QuizContract.QuestionsTable.COLUMN_ANSWER_NR + " INTEGER, " +
                QuizContract.QuestionsTable.COLUMN_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY(" + QuizContract.QuestionsTable.COLUMN_CATEGORY_ID + ") REFERENCES " +
                QuizContract.CategoriesTable.TABLE_NAME + "(" + QuizContract.CategoriesTable._ID + ")" + "ON DELETE CASCADE" +
                ")";
        final String SQL_CREATE_QUESTIONS_TABLE1 = "CREATE TABLE " +
                QuizContract.QuestionsTrueFalseTable.TABLE_NAME + " ( " +
                QuizContract.QuestionsTrueFalseTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuizContract.QuestionsTrueFalseTable.COLUMN_QUESTION + " TEXT, " +
                QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION1 + " TEXT, " +
                QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION2 + " TEXT, " +
                QuizContract.QuestionsTrueFalseTable.COLUMN_ANSWER_NR + " INTEGER, " +
                QuizContract.QuestionsTrueFalseTable.COLUMN_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY(" + QuizContract.QuestionsTrueFalseTable.COLUMN_CATEGORY_ID + ") REFERENCES " +
                QuizContract.CategoriesTable.TABLE_NAME + "(" + QuizContract.CategoriesTable._ID + ")" + "ON DELETE CASCADE" +
                ")";
        final String SQL_CREATE_QUESTIONS_TABLE2 = "CREATE TABLE " +
                QuizContract.QuestionsEnumerationTable.TABLE_NAME + " ( " +
                QuizContract.QuestionsEnumerationTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuizContract.QuestionsEnumerationTable.COLUMN_QUESTION + " TEXT, " +
                QuizContract.QuestionsEnumerationTable.COLUMN_OPTION1 + " TEXT, " +
                QuizContract.QuestionsEnumerationTable.COLUMN_ANSWER_NR + " TEXT, " +
                QuizContract.QuestionsEnumerationTable.COLUMN_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY(" + QuizContract.QuestionsEnumerationTable.COLUMN_CATEGORY_ID + ") REFERENCES " +
                QuizContract.CategoriesTable.TABLE_NAME + "(" + QuizContract.CategoriesTable._ID + ")" + "ON DELETE CASCADE" +
                ")";


        db.execSQL(SQL_CREATE_CATEGORIES_TABLE);
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE1);
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE2);
        fillCategoriesTable();
        fillMultipleChoiceQuestionsTable();
        fillTrueorFalseQuestionsTable();
        fillEnumerationQuestionsTable();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QuizContract.CategoriesTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuizContract.QuestionsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuizContract.QuestionsTrueFalseTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + QuizContract.QuestionsEnumerationTable.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private void fillCategoriesTable() {
        Category c1 = new Category("Multiple Choice");
        insertCategory(c1);
        Category c2 = new Category("True or False");
        insertCategory(c2);
        Category c3 = new Category("Enumeration");
        insertCategory(c3);
    }

    public void addCategory(Category category) {
        db = getWritableDatabase();
        insertCategory(category);
    }

    public void addCategories(List<Category> categories) {
        db = getWritableDatabase();

        for (Category category : categories) {
            insertCategory(category);
        }
    }

    private void insertCategory(Category category) {
        ContentValues cv = new ContentValues();
        cv.put(QuizContract.CategoriesTable.COLUMN_NAME, category.getName());
        db.insert(QuizContract.CategoriesTable.TABLE_NAME, null, cv);
    }

    private void fillMultipleChoiceQuestionsTable() {
        QuestionMultipleChoice q1 = new QuestionMultipleChoice("A is correct",
                "A", "B", "C", "D", 1,
                Category.MULTIPLECHOICE);
        insertQuestion(q1);
        QuestionMultipleChoice q2 = new QuestionMultipleChoice("B is correct",
                "A", "B", "C","D", 2,
                Category.MULTIPLECHOICE);
        insertQuestion(q2);
        QuestionMultipleChoice q3 = new QuestionMultipleChoice("C is correct",
                "A", "B", "C","D", 3,
                Category.MULTIPLECHOICE);
        insertQuestion(q3);
        QuestionMultipleChoice q4 = new QuestionMultipleChoice("A is correct",
                "A", "B", "C","D", 1,
                Category.MULTIPLECHOICE);
        insertQuestion(q4);
        QuestionMultipleChoice q5 = new QuestionMultipleChoice("D is correct",
                "A", "B", "C","D", 4,
                Category.MULTIPLECHOICE);
        insertQuestion(q5);
        QuestionMultipleChoice q6 = new QuestionMultipleChoice("B is correct",
                "A", "B", "C", "D", 2,
                Category.MULTIPLECHOICE);
        insertQuestion(q6);
        QuestionMultipleChoice q7 = new QuestionMultipleChoice("A is correct",
                "A", "B", "C", "D", 1,
                Category.MULTIPLECHOICE);
        insertQuestion(q7);
        QuestionMultipleChoice q8 = new QuestionMultipleChoice("B is correct",
                "A", "B", "C","D", 2,
                Category.MULTIPLECHOICE);
        insertQuestion(q8);
        QuestionMultipleChoice q9 = new QuestionMultipleChoice("C is correct",
                "A", "B", "C","D", 3,
                Category.MULTIPLECHOICE);
        insertQuestion(q9);
        QuestionMultipleChoice q10 = new QuestionMultipleChoice("A is correct",
                "A", "B", "C","D", 1,
                Category.MULTIPLECHOICE);
        insertQuestion(q10);


    }

    private void fillTrueorFalseQuestionsTable() {
        QuestionTrueFalse q1 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q1);
        QuestionTrueFalse q2 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q2);
        QuestionTrueFalse q3 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q3);
        QuestionTrueFalse q4 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q4);
        QuestionTrueFalse q5 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q5);
        QuestionTrueFalse q6 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q6);
        QuestionTrueFalse q7 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q7);
        QuestionTrueFalse q8 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q8);
        QuestionTrueFalse q9 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q9);
        QuestionTrueFalse q10 = new QuestionTrueFalse("A is correct",
                "True", "False",  1,
                Category.TRUEORFALSE);
        insertTrueFalseQuestion(q10);
    }

    private void fillEnumerationQuestionsTable() {
        QuestionEnumeration q1 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q1);
        QuestionEnumeration q2 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q2);
        QuestionEnumeration q3 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q3);
        QuestionEnumeration q4 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q4);
        QuestionEnumeration q5 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q5);
        QuestionEnumeration q6 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q6);
        QuestionEnumeration q7 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q7);
        QuestionEnumeration q8 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q8);
        QuestionEnumeration q9 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q9);
        QuestionEnumeration q10 = new QuestionEnumeration("A is correct",
                "A",   "A",
                Category.ENUMERATION);
        insertEnumerationQuestion(q10);

    }
    public void addQuestion(QuestionMultipleChoice question) {
        db = getWritableDatabase();
        insertQuestion(question);
    }

    public void addQuestions(List<QuestionMultipleChoice> questions) {
        db = getWritableDatabase();

        for (QuestionMultipleChoice question : questions) {
            insertQuestion(question);
        }
    }

    private void insertQuestion(QuestionMultipleChoice question) {
        ContentValues cv = new ContentValues();
        cv.put(QuizContract.QuestionsTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuizContract.QuestionsTable.COLUMN_OPTION1, question.getOption1());
        cv.put(QuizContract.QuestionsTable.COLUMN_OPTION2, question.getOption2());
        cv.put(QuizContract.QuestionsTable.COLUMN_OPTION3, question.getOption3());
        cv.put(QuizContract.QuestionsTable.COLUMN_OPTION4, question.getOption4());
        cv.put(QuizContract.QuestionsTable.COLUMN_ANSWER_NR, question.getAnswerNr());
        cv.put(QuizContract.QuestionsTable.COLUMN_CATEGORY_ID, question.getCategoryID());
        db.insert(QuizContract.QuestionsTable.TABLE_NAME, null, cv);
    }

    private void insertTrueFalseQuestion(QuestionTrueFalse question) {
        ContentValues cv = new ContentValues();
        cv.put(QuizContract.QuestionsTrueFalseTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION1, question.getOption1());
        cv.put(QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION2, question.getOption2());
        cv.put(QuizContract.QuestionsTrueFalseTable.COLUMN_ANSWER_NR, question.getAnswerNr());
        cv.put(QuizContract.QuestionsTrueFalseTable.COLUMN_CATEGORY_ID, question.getCategoryID());
        db.insert(QuizContract.QuestionsTrueFalseTable.TABLE_NAME, null, cv);
    }

    private void insertEnumerationQuestion(QuestionEnumeration question) {
        ContentValues cv = new ContentValues();
        cv.put(QuizContract.QuestionsEnumerationTable.COLUMN_QUESTION, question.getQuestion());
        cv.put(QuizContract.QuestionsEnumerationTable.COLUMN_OPTION1, question.getOption1());
        cv.put(QuizContract.QuestionsEnumerationTable.COLUMN_ANSWER_NR, question.getAnswerNr());
        cv.put(QuizContract.QuestionsEnumerationTable.COLUMN_CATEGORY_ID, question.getCategoryID());
        db.insert(QuizContract.QuestionsEnumerationTable.TABLE_NAME, null, cv);
    }

    public List<Category> getAllCategories() {
        List<Category> categoryList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + QuizContract.CategoriesTable.TABLE_NAME, null);

        if (c.moveToFirst()) {
            do {
                Category category = new Category();
                category.setId(c.getInt(c.getColumnIndex(QuizContract.CategoriesTable._ID)));
                category.setName(c.getString(c.getColumnIndex(QuizContract.CategoriesTable.COLUMN_NAME)));
                categoryList.add(category);
            } while (c.moveToNext());
        }

        c.close();
        return categoryList;
    }

    public ArrayList<QuestionMultipleChoice> getAllQuestions() {
        ArrayList<QuestionMultipleChoice> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + QuizContract.QuestionsTable.TABLE_NAME, null);

        if (c.moveToFirst()) {
            do {
                QuestionMultipleChoice question = new QuestionMultipleChoice();
                question.setId(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION3)));
                question.setOption4(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION4)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_ANSWER_NR)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }

        c.close();
        return questionList;
    }

    public ArrayList<QuestionMultipleChoice> getQuestions(int categoryID) {
        ArrayList<QuestionMultipleChoice> questionList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = QuizContract.QuestionsTable.COLUMN_CATEGORY_ID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(categoryID)};

        Cursor c = db.query(
                QuizContract.QuestionsTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                QuestionMultipleChoice question = new QuestionMultipleChoice();
                question.setId(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION3)));
                question.setOption4(c.getString(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_OPTION4)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_ANSWER_NR)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuizContract.QuestionsTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }

        c.close();
        return questionList;
    }
    public ArrayList<QuestionTrueFalse> getTrueFalseQuestions(int categoryID) {
        ArrayList<QuestionTrueFalse> questionList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = QuizContract.QuestionsTrueFalseTable.COLUMN_CATEGORY_ID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(categoryID)};

        Cursor c = db.query(
                QuizContract.QuestionsTrueFalseTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                QuestionTrueFalse question = new QuestionTrueFalse();
                question.setId(c.getInt(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable.COLUMN_OPTION2)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable.COLUMN_ANSWER_NR)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuizContract.QuestionsTrueFalseTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }

        c.close();
        return questionList;
    }
    public ArrayList<QuestionEnumeration> getEnumerationQuestions(int categoryID) {
        ArrayList<QuestionEnumeration> questionList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = QuizContract.QuestionsEnumerationTable.COLUMN_CATEGORY_ID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(categoryID)};

        Cursor c = db.query(
                QuizContract.QuestionsEnumerationTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                QuestionEnumeration question = new QuestionEnumeration();
                question.setId(c.getInt(c.getColumnIndex(QuizContract.QuestionsEnumerationTable._ID)));
                question.setQuestion(c.getString(c.getColumnIndex(QuizContract.QuestionsEnumerationTable.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(QuizContract.QuestionsEnumerationTable.COLUMN_OPTION1)));
                question.setAnswerNr(c.getString(c.getColumnIndex(QuizContract.QuestionsEnumerationTable.COLUMN_ANSWER_NR)));
                question.setCategoryID(c.getInt(c.getColumnIndex(QuizContract.QuestionsEnumerationTable.COLUMN_CATEGORY_ID)));
                questionList.add(question);
            } while (c.moveToNext());
        }

        c.close();
        return questionList;
    }
}