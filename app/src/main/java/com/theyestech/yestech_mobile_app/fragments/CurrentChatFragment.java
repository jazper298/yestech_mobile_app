package com.theyestech.yestech_mobile_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.MessageActivity;
import com.theyestech.yestech_mobile_app.adapters.EducatorAdapter;
import com.theyestech.yestech_mobile_app.adapters.StudentAdapter;
import com.theyestech.yestech_mobile_app.models.Chatlist;
import com.theyestech.yestech_mobile_app.models.Educator;
import com.theyestech.yestech_mobile_app.models.Student;
import com.theyestech.yestech_mobile_app.notifications.Token;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class CurrentChatFragment extends Fragment {
    private View view;
    private Context context;
    private EditText etSearch;
    private RecyclerView recyclerView;
    private FloatingActionButton fab_ChatNewConversation;

    private EducatorAdapter educatorAdapter ;
    private StudentAdapter studentAdapter;
    private ArrayList<Educator> mEducator;
    private ArrayList<Student> mStudent;
    private String role;

    private SwipeRefreshLayout swipeRefreshLayout;

    //Firebase
    FirebaseUser fuser;
    DatabaseReference reference;

    private ArrayList<Chatlist> educatorsCbatList;
    private ArrayList<Chatlist> studentCbatList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        role = getArguments().getString("ROLE");
        view = inflater.inflate(R.layout.fragment_current_chat, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (role.equals(UserType.Educator())) {
            initializeUI();
            getEducatorChatList();

        } else {
            initializeUI();
            getStudentChatList();
        }

    }

    private void initializeUI(){
        etSearch = view.findViewById(R.id.etSearch);
        swipeRefreshLayout = view.findViewById(R.id.swipe_Chats);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        swipeRefreshLayout.setRefreshing(true);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEducatorChatList();
            }
        });

        fab_ChatNewConversation = view.findViewById(R.id.fab_ChatNewConversation);

        fab_ChatNewConversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MessageActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });
    }


    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Educator----------------------------------------//

    private void getEducatorChatList() {
        fuser = FirebaseAuth.getInstance().getCurrentUser();

        educatorsCbatList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Chatlist").child(fuser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                educatorsCbatList.clear();
                swipeRefreshLayout.setRefreshing(false);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chatlist chatlist = snapshot.getValue(Chatlist.class);
                    educatorsCbatList.add(chatlist);
                }

                educatorChatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        updateEducatorToken(FirebaseInstanceId.getInstance().getToken());

    }

    private void updateEducatorToken(String token){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        reference.child(fuser.getUid()).setValue(token1);
    }

    private void educatorChatList() {
        mEducator = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Educator");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mEducator.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Educator educator = snapshot.getValue(Educator.class);
                    for (Chatlist chatlist : educatorsCbatList){
                        if (educator.getId().equals(chatlist.getId())){
                            mEducator.add(educator);
                            Debugger.logD("fuck " + educator);
                        }
                    }
                }
                educatorAdapter = new EducatorAdapter(getContext(), mEducator, true);
                recyclerView.setAdapter(educatorAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Student----------------------------------------//
    private void getStudentChatList() {
        fuser = FirebaseAuth.getInstance().getCurrentUser();

        studentCbatList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Chatlist").child(fuser.getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                studentCbatList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chatlist chatlist = snapshot.getValue(Chatlist.class);
                    studentCbatList.add(chatlist);
                }

                studentChatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        updateStudentToken(FirebaseInstanceId.getInstance().getToken());

    }

    private void updateStudentToken(String token){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1 = new Token(token);
        reference.child(fuser.getUid()).setValue(token1);
    }

    private void studentChatList() {
        mStudent = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Student");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mStudent.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Student student = snapshot.getValue(Student.class);
                    for (Chatlist chatlist : studentCbatList){
                        if (student.getId().equals(chatlist.getId())){
                            mStudent.add(student);
                            Debugger.logD("fuck " + student);
                        }
                    }
                }
                studentAdapter = new StudentAdapter(getContext(), mStudent, true);
                recyclerView.setAdapter(studentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
