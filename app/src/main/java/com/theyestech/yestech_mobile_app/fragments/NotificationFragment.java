package com.theyestech.yestech_mobile_app.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.NotificationListAdapter;
import com.theyestech.yestech_mobile_app.adapters.ViewPagerAdapter;
import com.theyestech.yestech_mobile_app.models.NotificationList;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.UserType;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private View view;
    private Context context;
    private String role;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private ArrayList<NotificationList> notificationLists;
    private NotificationListAdapter notificationListAdapter;
    //Widgets
    private ImageView iv_Ellipses;
    private RecyclerView recyclerView;
    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();
        role = UserType.getRole(context);
        initializeUI();

    }

    private void initializeUI(){


        //recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        iv_Ellipses = (ImageView)view.findViewById(R.id.iv_RankingMMore);

        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.view_pager);

        Bundle bundle = new Bundle();
        bundle.putString("ROLE", role);

        NotificationAllFragment notificationAllFragment = new NotificationAllFragment();
        notificationAllFragment.setArguments(bundle);

        NotificationRequestFragment notificationRequestFragment = new NotificationRequestFragment();
        notificationRequestFragment.setArguments(bundle);

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(notificationAllFragment, "A l l");


        Debugger.logD("adsasda" + bundle);
        viewPagerAdapter.addFragment( notificationRequestFragment, "R e q u e s t s");


        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);

        iv_Ellipses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEllipsesImportDialog();
            }
        });
        //showNotifications();
    }

    //Show Notification Dialog
    private void showEllipsesImportDialog(){
        String items[] = {"Mark All as Read", "Mute ", "Settings ", "Cancel "};
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(getContext());

        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0){

                }
                if (which == 1){

                }
                if (which == 2){

                }
                if (which == 3){
                    dialog.dismiss();
                }
            }
        });
        dialog.create().show();
    }

}
