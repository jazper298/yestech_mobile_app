package com.theyestech.yestech_mobile_app.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.MenuAboutActivity;
import com.theyestech.yestech_mobile_app.activities.MenuAssessmentActivity;
import com.theyestech.yestech_mobile_app.activities.MenuSettingsActivity;
import com.theyestech.yestech_mobile_app.activities.MenuNotesActivity;
import com.theyestech.yestech_mobile_app.activities.MenuPrivacyAndPolicyActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    private Context context;
    private View view;

    private ImageView ivLogout, iv_ProfileAbout, iv_ProfileAssessment, iv_ProfileLogout, iv_ProfileSettings;

    private CardView cvAbout, cvAssessment, cvNotes, cvPrivacyAndPolicy, cvSettings;


    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_menu, container, false);
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();
        initializeUI();
    }

    private void initializeUI() {
        cvAbout = view.findViewById(R.id.cv_MenuAbout);
        cvAssessment = view.findViewById(R.id.cv_MenuAssessment);
        cvNotes = view.findViewById(R.id.cv_MenuNotes);
        cvPrivacyAndPolicy = view.findViewById(R.id.cv_MenuPrivacyAndPolicy);
        cvSettings = view.findViewById(R.id.cv_MenuSettings);

        cvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MenuAboutActivity.class);
                startActivity(intent);
            }
        });
        cvAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MenuAssessmentActivity.class);
                startActivity(intent);
            }
        });
        cvSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MenuSettingsActivity.class);
                startActivity(intent);
            }
        });

        cvNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MenuNotesActivity.class);
                startActivity(intent);
            }
        });

        cvPrivacyAndPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MenuPrivacyAndPolicyActivity.class);
                startActivity(intent);
            }
        });
    }
}
