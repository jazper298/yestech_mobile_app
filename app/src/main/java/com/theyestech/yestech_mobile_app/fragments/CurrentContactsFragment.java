package com.theyestech.yestech_mobile_app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.ContactListAdapter;
import com.theyestech.yestech_mobile_app.adapters.EducatorAdapter;
import com.theyestech.yestech_mobile_app.adapters.StudentAdapter;
import com.theyestech.yestech_mobile_app.adapters.StudentsAdapter;
import com.theyestech.yestech_mobile_app.models.ContactList;
import com.theyestech.yestech_mobile_app.models.Educator;
import com.theyestech.yestech_mobile_app.models.Student;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class CurrentContactsFragment extends Fragment {
    private Context context;
    private View view;
    private RecyclerView recyclerView;
    private EditText etSearch;
    private ArrayList<ContactList> contactLists;
    private ContactListAdapter contactListAdapter;
    private String role;


    private EducatorAdapter educatorAdapter;
    private ArrayList<Educator> mEducators;

    private StudentAdapter studentAdapter;
    private  ArrayList<Student> mStudents;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        role = getArguments().getString("ROLE");
        view = inflater.inflate(R.layout.fragment_current_contacts, container, false);
        context = getContext();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (role.equals(UserType.Educator())) {
            initializeUI();
            initializeEducatorUI();
            //initializeStudentUI();

        } else {
            initializeUI();
            initializeStudentUI();
            //initializeEducatorUI();
        }

    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


    }

    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Educator----------------------------------------//

    private void initializeEducatorUI() {

        mEducators = new ArrayList<>();

        readAllEducators();
        etSearch = view.findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchAllEducators(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void searchAllEducators(String s) {

        final FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Educator").orderByChild("search")
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mEducators.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Educator user = snapshot.getValue(Educator.class);

                    assert user != null;
                    assert fuser != null;
                    if (!user.getId().equals(fuser.getUid())){
                        mEducators.add(user);
                    }
                }

                educatorAdapter = new EducatorAdapter(getContext(), mEducators, false);
                recyclerView.setAdapter(educatorAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void readAllEducators() {

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Educator");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (etSearch.getText().toString().equals("")) {
                    mEducators.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Educator educator = snapshot.getValue(Educator.class);

                        assert educator != null;
                        if (!educator.getId().equals(firebaseUser.getUid())) {
                            mEducators.add(educator);
                        }

                    }

                    educatorAdapter = new EducatorAdapter(getContext(), mEducators, false);
                    recyclerView.setAdapter(educatorAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Student----------------------------------------//

    private void initializeStudentUI() {

        mStudents = new ArrayList<>();

        readAllStudents();
        etSearch = view.findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchAllStudent(charSequence.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void searchAllStudent(String s) {

        final FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
        Query query = FirebaseDatabase.getInstance().getReference("Student").orderByChild("search")
                .startAt(s)
                .endAt(s+"\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mStudents.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Student student = snapshot.getValue(Student.class);

                    assert student != null;
                    assert fuser != null;
                    if (!student.getId().equals(fuser.getUid())){
                        mStudents.add(student);
                    }
                }

                studentAdapter = new StudentAdapter(getContext(), mStudents, false);
                recyclerView.setAdapter(studentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void readAllStudents() {

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Student");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (etSearch.getText().toString().equals("")) {
                    mStudents.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Student student = snapshot.getValue(Student.class);

                        assert student != null;
                        if (!student.getId().equals(firebaseUser.getUid())) {
                            mStudents.add(student);
                        }

                    }

                    studentAdapter = new StudentAdapter(getContext(), mStudents, false);
                    recyclerView.setAdapter(studentAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
