package com.theyestech.yestech_mobile_app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theyestech.yestech_mobile_app.R;


public class CreateQuizFragment extends Fragment {
    private View view;
    private Context context;
    private String quizTitle;
    private String quizType;
    private String quizItems;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        quizTitle = getArguments().getString("QuizTitle");
        quizType = getArguments().getString("QuizType");
        quizItems = getArguments().getString("QuizItems");

        context = getContext();

        if(quizType.equals("Multiple Choice")) {
            view = inflater.inflate(R.layout.fragment_create_quiz, container, false);
        }
        else if (quizType.equals("Enumeration")){
            view = inflater.inflate(R.layout.fragment_quiz_enumeration, container, false);
        }
        else if (quizType.equals("True or False")){
            view = inflater.inflate(R.layout.fragment_true_or_false, container, false);
        }
        else if(quizType.equals("Matching Type")){
            view = inflater.inflate(R.layout.fragment_matching_type, container, false);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
