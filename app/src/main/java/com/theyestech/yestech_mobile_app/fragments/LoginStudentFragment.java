package com.theyestech.yestech_mobile_app.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.MainActivity;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.RegisterStudentActivity;
import com.theyestech.yestech_mobile_app.activities.ResetPasswordActivity;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;

public class LoginStudentFragment extends Fragment  {
    //API LOGIN
    private View view;
    private Context context;
    private Button btnSignin;
    private ProgressDialog progressDialog;
    //private com.example.picpamobileapp.Models.UserProfile userProfile;

    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };


    public LoginStudentFragment() {

    }

    //Widgets
    private TextView tv_CreateAccount, tv_ForgotPassword;
    private Button btn_Next;
    private EditText etEmail, etPassword;
    private ProgressBar progressBar;
    private View loginView;

    private View mProgressView;
    private View mLoginFormView;
    private TextInputEditText input_Username;

    //Firebase
    FirebaseAuth auth;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login_student, container,false);
        context = getContext();
        initializeUI();
        return view;
    }
    private void initializeUI(){
//        mLoginFormView = view.findViewById(R.id.login_form);
//        mProgressView = view.findViewById(R.id.login_progress);
        tv_CreateAccount = view.findViewById(R.id.tv_CreateAccount);
        tv_ForgotPassword = view.findViewById(R.id.tv_ForgotPassword);
        progressBar = view.findViewById(R.id.login_progress);
        loginView = view.findViewById(R.id.email_login_form);
        tv_CreateAccount = view.findViewById(R.id.tv_CreateAccount);
        etEmail = view.findViewById(R.id.input_Username);
        etPassword = view.findViewById(R.id.et_LoginPassword);

        auth = FirebaseAuth.getInstance();

        btn_Next = view.findViewById(R.id.btn_Next);
        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fieldsAreEmpty()) {
                    Toasty.warning(context, "Please input email and password.").show();
                    etEmail.requestFocus();
                }
                else {
                    String email = etEmail.getText().toString();
                    String password = etPassword.getText().toString();
                    RequestParams params = new RequestParams();
                    params.put("login_s_email_address", email);
                    params.put("login_s_password", password);

                    doLoginStudent(params);
                }

            }
        });

        tv_CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RegisterStudentActivity.class);
                startActivity(intent);
            }
        });
        tv_ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ResetPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doLoginStudent(RequestParams params) {

        showLoading(true);
        HttpProvider.postLogin(context, "controller_student/login_as_student_class.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    if (jsonArray.toString().contains("success_student")) {
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        UserSessionStudent sessionStudent = new Gson().fromJson(jsonObject.toString(), new TypeToken<UserSessionStudent>() {
                        }.getType());
                        doFirebaseLoginStudent(sessionStudent);

                    } else {
                        Toasty.warning(context, "Incorrect email or password.").show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showLoading(false);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                showLoading(false);
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }

        });
    }

    private boolean fieldsAreEmpty() {
        if (etEmail.getText().toString().isEmpty() && etPassword.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        loginView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public static LoginStudentFragment newInstance() {
        LoginStudentFragment fragment = new LoginStudentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    //-----------------------------------------Firebase----------------------------------------//

    private void doFirebaseLoginStudent(final UserSessionStudent userSessionStudent) {
        String txt_email = etEmail.getText().toString();
        String txt_password = etPassword.getText().toString();

        if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
            Toast.makeText(context, "All fileds are required", Toast.LENGTH_SHORT).show();
        } else {

            auth.signInWithEmailAndPassword(txt_email, txt_password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                String firebaseUser = FirebaseAuth.getInstance().getUid();
                                userSessionStudent.setFirebaseToken(firebaseUser);
                                userSessionStudent.saveUserSession(context);
                                KeyboardHandler.closeKeyboard(view, context);
                                Toasty.success(context, "Success.").show();
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
//                                finish();
                            } else {
                                Toast.makeText(context, "Authentication failed!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
}
