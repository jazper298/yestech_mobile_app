package com.theyestech.yestech_mobile_app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.NotificationListAdapter;
import com.theyestech.yestech_mobile_app.models.NotificationList;

import java.util.ArrayList;

public class NotificationAllFragment extends Fragment {

    private View view;
    private Context context;
    private EditText etSearch;
    private RecyclerView recyclerView;

    private ArrayList<NotificationList> notificationLists;
    private NotificationListAdapter notificationListAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification_all, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
        showNotifications();
    }

    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
    }

    //Show Notification List
    private void showNotifications(){
        notificationLists = new ArrayList<>();

        notificationLists.add(new NotificationList(R.drawable.john1, R.drawable.ic_videocam, R.drawable.soo_in, R.drawable.ic_more_horiz, "John Petrucci", "Getting a null pointer exception tabs! ", "12h"));
        notificationLists.add(new NotificationList(R.drawable.save1, R.drawable.ic_image, R.drawable.yes_logo, R.drawable.ic_more_horiz, "Dave Mustaine", "I am trying to setup icons in the tabs", "3d"));
        notificationLists.add(new NotificationList(R.drawable.yngwie1, R.drawable.ic_thumb_up, R.drawable.joe1, R.drawable.ic_more_horiz, "Yngwie Malmstein", "Likes your activity and John Petrucci", "15mins"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        notificationListAdapter = new NotificationListAdapter(getContext(), notificationLists);

        recyclerView.setAdapter(notificationListAdapter);
    }
}
