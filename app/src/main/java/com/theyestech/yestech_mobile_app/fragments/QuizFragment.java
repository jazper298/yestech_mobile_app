package com.theyestech.yestech_mobile_app.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.QuizActivity;
import com.theyestech.yestech_mobile_app.activities.QuizListActivity;
import com.theyestech.yestech_mobile_app.activities.SubjectDescActivity;
import com.theyestech.yestech_mobile_app.adapters.ProfileSubjectsAdapter;
import com.theyestech.yestech_mobile_app.adapters.QuizSubjectsAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Subjects;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment {

    private Context context;
    private View view;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView ivAdd;
    private RecyclerView rv_Quiz;
    private EditText etSearch;

    private ProfileSubjectsAdapter profileSubjectsAdapter;
    private Subjects selected_subjects;


    private String role;

    private ArrayList<String> section_ids, section_names;

    private ProgressDialog progressDialog;


    private ArrayList<Subjects> subjectsArrayList = new ArrayList<>();
    private QuizSubjectsAdapter quizSubjectsAdapter;

    private int numberOfColumns = 2;

    public QuizFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_quiz, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        KeyboardHandler.hideKeyboard(getActivity());
        context = getContext();
        role = UserType.getRole(context);

        section_ids = new ArrayList<>();
        section_names = new ArrayList<>();
        initializeUI();

        swipeRefreshLayout.setRefreshing(false);
    }

    private void initializeUI() {
        rv_Quiz = view.findViewById(R.id.rv_Quiz);
        swipeRefreshLayout = view.findViewById(R.id.swipe_Quiz);
        ivAdd = view.findViewById(R.id.iv_QuizAdd);

        if (role.equals(UserType.Student())) {
            ivAdd.setVisibility(View.GONE);
            //loadStudentSubjects();
            loadDummyStudentSubjects();
        } else {
            loadEducatorSubjects();
        }
    }

    private void loadEducatorSubjects() {
        swipeRefreshLayout.setRefreshing(true);

        subjectsArrayList.clear();
        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/get_subjects.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String sub_id = jsonObject.getString("subj_id");
                        String sub_title = jsonObject.getString("subj_title");

                        Subjects items = new Subjects();
                        items.setSubject_id(sub_id);
                        items.setSub_title(sub_title);
                        subjectsArrayList.add(items);
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                    rv_Quiz.setLayoutManager(rvLayoutManager);
                    rv_Quiz.setLayoutManager(new GridLayoutManager(context, numberOfColumns));

                    profileSubjectsAdapter = new ProfileSubjectsAdapter(context, subjectsArrayList);
                    profileSubjectsAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selected_subjects = subjectsArrayList.get(position);

                            PopupMenu popup = new PopupMenu(context, view);
                            popup.inflate(R.menu.menu_quiz_list);

                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.view_quiz:
                                            Intent intent = new Intent(context, QuizListActivity.class);
                                            intent.putExtra("SUBJECT_ID", selected_subjects.getSubject_id());
                                            startActivity(intent);
                                            return true;
                                    }
                                    return false;
                                }
                            });
                            popup.show();

                        }
                    });

                    rv_Quiz.setAdapter(profileSubjectsAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                swipeRefreshLayout.setRefreshing(false);
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });

    }

    private void loadStudentSubjects() {
        swipeRefreshLayout.setRefreshing(true);

        subjectsArrayList.clear();
        RequestParams params = new RequestParams();
        params.put("stud_id", UserSessionStudent.getID(context));
        params.put("stud_token", UserSessionStudent.getToken(context));

        HttpProvider.defaultPost(context, "controller_student/get_student_subjects.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String sub_id = jsonObject.getString("subj_id");
                        String sub_title = jsonObject.getString("subj_title");

                        Subjects items = new Subjects();
                        items.setSubject_id(sub_id);
                        items.setSub_title(sub_title);
                        subjectsArrayList.add(items);
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                    rv_Quiz.setLayoutManager(rvLayoutManager);
                    rv_Quiz.setLayoutManager(new GridLayoutManager(context, numberOfColumns));

                    profileSubjectsAdapter = new ProfileSubjectsAdapter(context, subjectsArrayList);
                    profileSubjectsAdapter.setClickListener(new OnClickRecyclerView() {

                        @Override
                        public void onItemClick(View view, int position) {
                            selected_subjects = subjectsArrayList.get(position);
//                            Toast.makeText(context,"The Item Clicked is: "+ position,Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(context, SubjectDescActivity.class);
                            intent.putExtra("SUBJECT_ID", selected_subjects.getSubject_id());
                            startActivity(intent);
                        }
                    });

                    rv_Quiz.setAdapter(profileSubjectsAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                swipeRefreshLayout.setRefreshing(false);
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });

    }

    //Dummy lang sa
    private void loadDummyStudentSubjects() {
        subjectsArrayList = new ArrayList<>();
        String sub_id = "1";
        String sub_title = "English";

        Subjects items = new Subjects();
        items.setSubject_id(sub_id);
        items.setSub_title(sub_title);
        subjectsArrayList.add(items);


        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        rv_Quiz.setLayoutManager(rvLayoutManager);
        rv_Quiz.setLayoutManager(new GridLayoutManager(context, numberOfColumns));

        profileSubjectsAdapter = new ProfileSubjectsAdapter(context, subjectsArrayList);
        profileSubjectsAdapter.setClickListener(new OnClickRecyclerView() {

            @Override
            public void onItemClick(View view, int position) {
                selected_subjects = subjectsArrayList.get(position);

                PopupMenu popup = new PopupMenu(context, view);
                popup.inflate(R.menu.menu_quiz_list);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.view_quiz:
                                //openAddQuiz();
                                Intent intent = new Intent(context, QuizListActivity.class);
                                intent.putExtra("ROLE", role);
                                startActivity(intent);
                                return true;
                        }
                        return false;
                    }
                });
                popup.show();

            }
        });
        rv_Quiz.setAdapter(profileSubjectsAdapter);
    }


    private void openAddQuiz() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_create_quiz, null);
        final Button btn_CreateQuiz;
        final EditText et_CreateQuizTitle;
        final Spinner sp_SelectLesson, sp_QuizType, sp_QuizItems;

        et_CreateQuizTitle = dialogView.findViewById(R.id.et_CreateQuizTitle);
        btn_CreateQuiz = dialogView.findViewById(R.id.btn_CreateQuiz);
        sp_SelectLesson = dialogView.findViewById(R.id.sp_SelectLesson);
        sp_QuizType = dialogView.findViewById(R.id.sp_QuizType);
        sp_QuizItems = dialogView.findViewById(R.id.sp_QuizItems);


        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        btn_CreateQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quizTitle = et_CreateQuizTitle.getText().toString();

                Intent intent = new Intent(context, QuizActivity.class);
                intent.putExtra("QuizTitle", quizTitle);
                startActivity(intent);
                b.hide();
            }
        });


        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
