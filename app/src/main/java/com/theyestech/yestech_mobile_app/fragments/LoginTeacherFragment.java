package com.theyestech.yestech_mobile_app.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.MainActivity;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.LoginActivity;
import com.theyestech.yestech_mobile_app.activities.RegisterTeacherActivity;
import com.theyestech.yestech_mobile_app.activities.ResetPasswordActivity;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class LoginTeacherFragment extends Fragment {

    private View view;
    private Context context;
    private Button btnSignIn;

    private ProgressDialog progressDialog;
    //private com.example.picpamobileapp.Models.UserProfile userProfile;

    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };


    TextView forgot_password;

    //Firebase
    FirebaseAuth auth;

    public LoginTeacherFragment() {

    }

    //Widgets
    private TextView tv_CreateAccount,tv_ForgotPassword;
    private Button btn_Next;
    private EditText etEmail, etPassword;
    private ProgressBar progressBar;
    private View loginView;

    private View mProgressView;
    private View mLoginFormView;
    private TextInputEditText input_Username;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login_teacher, container,false);
        context = getContext();
        initializeUI();
        return view;
    }
    private void initializeUI(){

        auth = FirebaseAuth.getInstance();

        tv_ForgotPassword = view.findViewById(R.id.tv_ForgotPassword);
        progressBar = view.findViewById(R.id.login_progress);
        loginView = view.findViewById(R.id.email_login_form);
        tv_CreateAccount = view.findViewById(R.id.tv_CreateAccount);
        etEmail = view.findViewById(R.id.input_Username);
        etPassword = view.findViewById(R.id.et_LoginPassword);

        btn_Next = view.findViewById(R.id.btn_Next);
        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fieldsAreEmpty()) {
                    Toasty.warning(context, "Please input email and password.").show();
                    etEmail.requestFocus();
                }
                else {
                    String email = etEmail.getText().toString();
                    String password = etPassword.getText().toString();
                    RequestParams params = new RequestParams();
                    params.put("login_e_email_address", email);
                    params.put("login_e_password", password);

                    doLoginEducator(params);
                }
            }
        });
        tv_CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), RegisterTeacherActivity.class);
                startActivity(intent);
            }
        });
        tv_ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ResetPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doLoginEducator(final RequestParams params) {

        showLoading(true);
        HttpProvider.postLogin(context, "controller_educator/login_as_educator_class.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD("EDUCATOR_LOGIN: " + jsonArray.toString());
                    if (jsonArray.toString().contains("success_educator")) {
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        UserSessionEducator sessionEducator = new Gson().fromJson(jsonObject.toString(), new TypeToken<UserSessionEducator>() {
                        }.getType());
                        Debugger.logD("ID: " + sessionEducator.getTeachID());
                        getEducatorDetails(sessionEducator.getTeachID(), sessionEducator.getTeachToken());
                    } else {
                        Toasty.warning(context, "Incorrect email or password.").show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showLoading(false);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                showLoading(false);
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }

        });
    }

    private boolean fieldsAreEmpty() {
        if (etEmail.getText().toString().isEmpty() && etPassword.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        loginView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public static LoginTeacherFragment newInstance() {
        LoginTeacherFragment fragment = new LoginTeacherFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    //Educator Details
    private void getEducatorDetails(String id, String token){
        RequestParams params = new RequestParams();
        params.put("teach_id", id);
        params.put("teach_token", token);

        HttpProvider.defaultPost(context, "controller_educator/get_educator_details.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    if (jsonArray.toString().contains("success_educator")) {
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        UserSessionEducator sessionEducator = new Gson().fromJson(jsonObject.toString(), new TypeToken<UserSessionEducator>() {
                        }.getType());
//                        sessionEducator.saveUserSession(context);
//                        Intent main = new Intent(context, MainActivity.class);
//                        startActivity(main);
//                        Toasty.success(context, "Success.").show();
//                        getActivity().finish();
                        doFirebaseLoginEducator(sessionEducator);
                    } else {
                        Toasty.warning(context, "Incorrect email or password.").show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("SHITTA" + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    //-----------------------------------------Firebase----------------------------------------//

    private void doFirebaseLoginEducator(final UserSessionEducator userSessionEducator) {
        String txt_email = etEmail.getText().toString();
        String txt_password = etPassword.getText().toString();

        if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
            Toast.makeText(context, "All fileds are required", Toast.LENGTH_SHORT).show();
        } else {

            auth.signInWithEmailAndPassword(txt_email, txt_password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                String firebaseUser = FirebaseAuth.getInstance().getUid();
                                userSessionEducator.setFirebaseToken(firebaseUser);
                                userSessionEducator.saveUserSession(context);
                                KeyboardHandler.closeKeyboard(view, context);
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Debugger.printO(userSessionEducator);
                                //finish();
                            } else {
                                Toast.makeText(context, "Authentication failed!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
}
