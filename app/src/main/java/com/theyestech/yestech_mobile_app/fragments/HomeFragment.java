package com.theyestech.yestech_mobile_app.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.ChatActivity;
import com.theyestech.yestech_mobile_app.activities.ProfileActivity;
import com.theyestech.yestech_mobile_app.adapters.HomePostsAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Educator;
import com.theyestech.yestech_mobile_app.models.Posts;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.theyestech.yestech_mobile_app.utils.UserType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private View view;
    private Context context;
    //Widgets
    private ImageView ivImage, ivSearch, ivAdd, ivChat;
    private TextView tvName;
    private RecyclerView rvHome;
    private String role;

    private boolean connected = false;

    private ArrayList<Posts> postsArrayList = new ArrayList<>();
    private HomePostsAdapter homePostsAdapter;

    private ProgressDialog progressDialog;

    private static final int REQUEST_CAMERA = 5;
    private static final int SELECT_FILE = 0;

    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private String cameraPermission[];
    private String storagePermission[];
    private Uri image_uri;

    private List<Bitmap> mL;
    private List<Uri> mL1;
    private Uri selectImageUrl;
    private Bitmap imageBitmap;

    private ImageView iv_PostCam;
    private EditText et_PostContent;
    private String mCurrentPhotoPath;

    private SwipeRefreshLayout swipeRefreshLayout;

    private Posts selected_post = new Posts();

    String picturePath;
    private File finalFile = new File("");

    public HomeFragment() {
        // Required empty public constructor
    }

    //Firebase
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    byte[] bitmapdata;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        KeyboardHandler.hideKeyboard(getActivity());

        context = getContext();

        role = UserType.getRole(context);

//        isNetworkAvailable();

//        promptNetworkNotif();


        //setUserName();

        if (role.equals(UserType.Educator())) {
            initializeUI();
            ivAdd.setImageResource(R.drawable.ic_new_post_black);
            LoadHomeForEducator();
            getFirebaseEducator();
        } else {
            initializeUI();
            LoadHomeForStudent();
            getFirebaseStudent();
        }


    }

    private void isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
    }

    private void promptNetworkNotif(){
        if (!connected) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_join_subject, null);
            final EditText etCode;
            final Button btnSend;

            etCode = dialogView.findViewById(R.id.et_JoinSubjectCode);
            btnSend = dialogView.findViewById(R.id.btn_JoinSubjectSend);

            dialogBuilder.setView(dialogView);
            final AlertDialog b = dialogBuilder.create();

            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String code = etCode.getText().toString();

                    if (code.isEmpty()) {
                        Toasty.warning(context, "Please input subject code.").show();
                    } else {
                        sendCodeToEducator(b, code);
                    }
                }
            });

            b.show();
            b.setCancelable(false);
            Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } else {
            return;
        }
    }

    private void initializeUI() {
        tvName = view.findViewById(R.id.tv_HomeName);
        ivSearch = view.findViewById(R.id.iv_HomeSearch);
        ivAdd = view.findViewById(R.id.iv_HomeAddSubject);
        ivImage = view.findViewById(R.id.iv_HomeImage);
        rvHome = view.findViewById(R.id.rv_Home);
        ivChat = view.findViewById(R.id.iv_HomeChat);
        swipeRefreshLayout = view.findViewById(R.id.swipe_home);

        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadHomeForEducator();
            }
        });

        Glide.with(context)
                .load(R.drawable.ic_profile_accent)
                .apply(GlideOptions.getGlideOptions())
                .into(ivImage);

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.warning(context, "Search button clicked.").show();
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (role.equals(UserType.Student())) {
                    addForStudent();
                } else {
//                    addForEducator(v);
                    //openAddPost();
                    addForEducator(v);
                }
            }
        });

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                intent.putExtra("ROLE", role);
                startActivity(intent);
            }
        });

        ivChat.setImageResource(R.drawable.ic_chat);

        ivChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra("ROLE", role);
                startActivity(intent);
            }
        });

    }

    private void setUserName() {
        if (role.equals(UserType.Educator())) {
            if (!UserSessionEducator.getFirstname(context).equals("")) {
                tvName.setText(UserSessionEducator.getFirstname(context));
            } else {
                tvName.setText(role);
            }
        } else {
            if (!UserSessionStudent.getFirstname(context).equals("")) {
                tvName.setText(UserSessionStudent.getFirstname(context));
            } else {
                tvName.setText(role);
            }
        }
    }

    //Open Add Subject Dialog
    private void addForStudent() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_join_subject, null);
        final EditText etCode;
        final Button btnSend;

        etCode = dialogView.findViewById(R.id.et_JoinSubjectCode);
        btnSend = dialogView.findViewById(R.id.btn_JoinSubjectSend);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etCode.getText().toString();

                if (code.isEmpty()) {
                    Toasty.warning(context, "Please input subject code.").show();
                } else {
                    sendCodeToEducator(b, code);
                }
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void addForEducator(View v) {

        final Context context=v.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_add_post, null);

        final ImageView iv_HomeImage = view.findViewById(R.id.iv_HomeImage);
        final TextView tv_HomeName = view.findViewById(R.id.tv_HomeName);
        final Button btn_Post = view.findViewById(R.id.btn_Post);
        et_PostContent = view.findViewById(R.id.et_PostContent);
        final ImageView iv_AddPostCam = view.findViewById(R.id.iv_AddPostCam);
        iv_PostCam = view.findViewById(R.id.iv_PostCam);

        //ImageView imageView = (ImageView)view.findViewById(R.id.imageView);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Educator educator = dataSnapshot.getValue(Educator.class);
                assert educator != null;
                tv_HomeName.setText(educator.getUsername());
                if (educator.getImageURL().equals("default")) {
                    iv_HomeImage.setImageResource(R.mipmap.ic_launcher);
                } else {
                    //change this
                    Glide.with(context)
                            .load(educator.getImageURL())
                            .apply(GlideOptions.getGlideOptions())
                            .into(iv_HomeImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final Dialog mBottomSheetDialog = new Dialog (context);
        Window window = mBottomSheetDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // BottomSheet

        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.CENTER);
        mBottomSheetDialog.show ();

        cameraPermission = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        iv_AddPostCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageImportDialog();
            }
        });
        btn_Post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = et_PostContent.getText().toString();
                try {
                    savePost(title, mBottomSheetDialog);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void savePost(String title, final Dialog dialog) throws IOException {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

//        File myFile = new File(picturePath);
//        byte[] fileData = new byte[(int) myFile.length()];

//        File file = new File(picturePath);
//        //init array with file length
//        byte[] bytesArray = new byte[(int) file.length()];
//
//        FileInputStream fis = new FileInputStream(file);
//        fis.read(bytesArray); //read file into bytes[]
//        fis.close();
//
//        Debugger.printO(bytesArray);

        RequestParams params = new RequestParams();
        params.put("nf_details", title);
        params.put("nf_file", "");
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/upload_post.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                Debugger.printO(responseBody);
                dialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.printO("SAVE POSTS: " + jsonArray);
//                    JSONObject jsonObject = new JSONObject(new String(responseBody));
//                    Debugger.printO("SAVE POSTS: " + jsonObject);
//                    JSONObject jsonObject = jsonArray.getJSONObject(0);
//                    Debugger.logD("SAVE POST: " + jsonObject.toString());
//                    if (jsonArray.toString().contains("success")) {
//                        Toasty.success(context, "Post Saved.").show();
//                        //loadProfileSubjects();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("POST ERROR: " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void sendCodeToEducator(final AlertDialog b, String code) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("stud_id", UserSessionStudent.getID(context));
        params.put("stud_token", UserSessionStudent.getToken(context));
        params.put("subj_code", code);

        HttpProvider.defaultPost(context, "controller_student/request_join_subject.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                b.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));

                    Toasty.success(context, "Request sent.").show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void LoadHomeForStudent() {

//        progressDialog = new ProgressDialog(context);
//        progressDialog.setTitle("Please wait");
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//
//        RequestParams params = new RequestParams();
//        params.put("teach_id", UserSessionEducator.getID(context));
//        params.put("teach_token", UserSessionEducator.getToken(context));
//
//        HttpProvider.defaultPost(context, "controller_educator/get_sections.php", params, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                progressDialog.dismiss();
//                try {
//                    JSONArray jsonArray = new JSONArray(new String(responseBody));
//                    Debugger.logD(jsonArray.toString());
//                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
//                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        String id = jsonObject.getString("section_id");
//                        String name = jsonObject.getString("section_name");
//                        String year = jsonObject.getString("section_year");
//
//                        Posts posts = new Sections(id, name, year);
//
//                        postsArrayList.add(sections);
//                    }
//
//                    recyclerView.setLayoutManager(new LinearLayoutManager(ProfileSectionsActivity.this));
//                    recyclerView.setHasFixedSize(true);
//                    sectionsAdapter = new SectionsAdapter(context, sectionsArrayList);
//                    sectionsAdapter.setClickListener(new OnClickRecyclerView() {
//                        @Override
//                        public void onItemClick(View view, int position) {
//                            selected_section = sectionsArrayList.get(position);
//                            isEdit = true;
//
//                            PopupMenu popup = new PopupMenu(context, view);
//                            popup.inflate(R.menu.edit_delete_popup_menu);
//
//                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                                @Override
//                                public boolean onMenuItemClick(MenuItem item) {
//                                    switch (item.getItemId()) {
//                                        case R.id.popup_edit:
//                                            openEditSection();
//                                            return true;
//                                        case R.id.popup_delete:
//                                            openDeleteSection();
//                                            return true;
//                                    }
//                                    return false;
//                                }
//                            });
////                            openEditSection();
//                            popup.show();
//                        }
//                    });
//                    recyclerView.setAdapter(sectionsAdapter);
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Debugger.logD(e.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                progressDialog.dismiss();
//                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
//
//            }
//        });


    }

    private void LoadHomeForEducator(){
        postsArrayList.clear();

        RequestParams params = new RequestParams();
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/get_post.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                swipeRefreshLayout.setRefreshing(false);
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.printO("POSTS: " + jsonArray);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String nf_id = jsonObject.getString("nf_id");
                        String nf_token = jsonObject.getString("nf_token");
                        String nf_user_token = jsonObject.getString("nf_user_token");
                        String nf_files = jsonObject.getString("nf_files");
                        String nf_details = jsonObject.getString("nf_details");
                        String nf_date = jsonObject.getString("nf_date");
                        String nf_fullname = jsonObject.getString("nf_fullname");
                        String nf_image = jsonObject.getString("nf_image");

                        Posts posts = new Posts();
                        posts.setNf_id(nf_id);
                        posts.setNf_token(nf_token);
                        posts.setNf_user_token(nf_user_token);
                        posts.setNf_files(nf_files);
                        posts.setNf_details(nf_details);
                        posts.setNf_date(nf_date);
                        posts.setNf_fullname(nf_fullname);
                        posts.setNf_image(nf_image);

                        postsArrayList.add(posts);
                    }

                    rvHome.setLayoutManager(new LinearLayoutManager(context));
                    rvHome.setHasFixedSize(true);
                    homePostsAdapter = new HomePostsAdapter(context, postsArrayList);
                    homePostsAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selected_post = postsArrayList.get(position);
                        }
                    });
                    rvHome.setAdapter(homePostsAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.printError(e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                swipeRefreshLayout.setRefreshing(false);
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void showImageImportDialog(){
        String items[] = {" Camera ", " Gallery ", "Cancel"};
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(context);
        dialog.setTitle("Select Image");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0){
                    if (!checkCameraPermission()){
                        requestCameraPermission();
                    }else{
                        pickCamera();
                    }
                }
                if (which == 1){
                    if(!checkStoragePermission()){
                        requestStoragePermission();
                    }else{
                        pickGallery();
                    }
                }
                if (which == 2) {
                    dialog.dismiss();
                }
            }
        });
        dialog.create().show();
    }
    private void pickGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//            File photoFile = null;
//            try{
//                photoFile = createImageFIle();
//            }
//            catch (IOException ex){
//                Toasty.error(context, "Something went wrong." + ex.getMessage()).show();
//            }
//            if (photoFile != null) {
//                Uri photoUri = FileProvider.getUriForFile(context, "com.example.yestech_mobile_app.fileprovider", photoFile);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
           // }
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(getActivity(), storagePermission, STORAGE_REQUEST_CODE);
    }
    private boolean checkStoragePermission() {
        boolean result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return  result2;
    }
    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(getActivity(), cameraPermission, CAMERA_REQUEST_CODE);
    }
    private boolean checkCameraPermission(){
        boolean result = ContextCompat.checkSelfPermission(context,Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result2 = ContextCompat.checkSelfPermission(context,Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result2;
    }

    private File createImageFIle() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpeg"
                //storageDir
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted && writeStorageAccepted){
                        pickCamera();
                    }else{
                        Toasty.error(context, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case STORAGE_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if( writeStorageAccepted){
                        pickGallery();
                    }else{
                        Toasty.error(context, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            //imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            iv_PostCam.setImageBitmap(imageBitmap);

            Uri tempUri = getImageUri(getContext(), imageBitmap);

            finalFile = new File(getRealPathFromURI(tempUri));

            picturePath = finalFile.toString();
        }
        else if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_GALLERY_CODE)
        {
            selectImageUrl = data.getData();
            iv_PostCam.setImageURI(selectImageUrl);

            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContext().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Educator----------------------------------------//

    //Firebase User
    private void getFirebaseEducator() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Educator educator = dataSnapshot.getValue(Educator.class);
                assert educator != null;
                tvName.setText(educator.getUsername());
                if (educator.getImageURL().equals("default")) {
                    Glide.with(context)
                            .load(R.drawable.ic_profile_accent)
                            .apply(GlideOptions.getGlideOptions())
                            .into(ivImage);
                } else {

                    //change this 
                    Glide.with(context)
                            .load(educator.getImageURL())
                            .apply(GlideOptions.getGlideOptions())
                            .into(ivImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void statusEducator(String status) {
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(firebaseUser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }


    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Student----------------------------------------//

    //Firebase User
    private void getFirebaseStudent() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Student").child(firebaseUser.getUid());


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Educator educator = dataSnapshot.getValue(Educator.class);
                assert educator != null;
                tvName.setText(educator.getUsername());
                if (educator.getImageURL().equals("default")) {
                    ivImage.setImageResource(R.mipmap.ic_launcher);
                } else {

                    //change this
//                    Glide.with(Objects.requireNonNull(context)).load(educator.getImageURL()).into(ivImage);
                    RequestOptions myOption = new RequestOptions()
                            .centerInside()
                            .circleCrop();

                    Glide.with(Objects.requireNonNull(context))
                            .load(educator.getImageURL())
                            .apply(myOption)
                            .into(ivImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void statusStudent(String status) {
        reference = FirebaseDatabase.getInstance().getReference("Student").child(firebaseUser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (role.equals(UserType.Educator())) {
            statusEducator("online");
        } else {
            statusStudent("online");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (role.equals(UserType.Educator())) {
            statusEducator("offline");
        } else {
            statusStudent("offline");
        }
    }

}
