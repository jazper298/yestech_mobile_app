package com.theyestech.yestech_mobile_app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.RankingsAdapter;
import com.theyestech.yestech_mobile_app.models.Rankings;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class RankingsFragment extends Fragment {

    private Context context;
    private View view;

    private String role;
    private TabLayout tabLayout;
    private RecyclerView recyclerView;
    private Spinner spSubjects;
    private Button btnFilter;
    private CardView cv1, cv2, cv3;
    private TextView tvIndicator;

    private String selectedTab;
    private String selectedSubjectId;

    private ImageView ivProfile1, ivProfile2, ivProfile3;
    private TextView tvName1, tvName2, tvName3, tvScore1, tvScore2, tvScore3;

    private RankingsAdapter rankingsAdapter;
    private ArrayList<Rankings> rankingsArrayList = new ArrayList<>();

    private ProgressDialog progressDialog;

    private ArrayList<String> subject_ids, subject_names;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_rankings, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();

        role = UserType.getRole(context);

        selectedTab = "Today";

        subject_ids = new ArrayList<>();
        subject_names = new ArrayList<>();

        initializeUI();
    }

    public void initializeUI() {
        tabLayout = view.findViewById(R.id.tab_Ranking);
        recyclerView = view.findViewById(R.id.rv_Rankings);
        spSubjects = view.findViewById(R.id.sp_RankingSubjects);
        btnFilter = view.findViewById(R.id.btn_RankingFilter);
        cv1 = view.findViewById(R.id.cv_Ranking1);
        cv2 = view.findViewById(R.id.cv_Ranking2);
        cv3 = view.findViewById(R.id.cv_Ranking3);
        tvIndicator = view.findViewById(R.id.tv_RankingIdicator);

        ivProfile1 = view.findViewById(R.id.iv_RankingProfile1);
        ivProfile2 = view.findViewById(R.id.iv_RankingProfile2);
        ivProfile3 = view.findViewById(R.id.iv_RankingProfile3);
        tvName1 = view.findViewById(R.id.tv_RankingName1);
        tvName2 = view.findViewById(R.id.tv_RankingName2);
        tvName3 = view.findViewById(R.id.tv_RankingName3);
        tvScore1 = view.findViewById(R.id.tv_RankingScore1);
        tvScore2 = view.findViewById(R.id.tv_RankingScore2);
        tvScore3 = view.findViewById(R.id.tv_RankingScore3);

        spSubjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tabLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (role.equals(UserType.Educator())){
            loadEducatorSubjects();
        } else {

        }

    }

    private void loadEducatorSubjects() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        subject_ids.clear();
        subject_names.clear();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/get_subjects.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        subject_ids.add(jsonObject.getString("subj_id"));
                        subject_names.add(jsonObject.getString("subj_title"));
                    }

                    if (jsonArray.length() != 0) {
                        ArrayAdapter<String> sectionAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, subject_names);
                        spSubjects.setAdapter(sectionAdapter);

                        selectedSubjectId = subject_ids.get(0);
                        selectedTab = "Today";

                        loadRankings(selectedTab, selectedSubjectId);
                        loadTopRankings(selectedTab, selectedSubjectId);

                        HideShowRanking(true);
                    } else {
                        HideShowRanking(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void loadTopRankings(String tab, String subjectId) {
        Glide.with(Objects.requireNonNull(context))
                .load(R.drawable.sample_01)
                .apply(GlideOptions.getGlideOptions())
                .into(ivProfile1);
        tvName1.setText("Vanne");
        tvScore1.setText("100 pts");

        Glide.with(Objects.requireNonNull(context))
                .load(R.drawable.sample_04)
                .apply(GlideOptions.getGlideOptions())
                .into(ivProfile2);
        tvName2.setText("Michael");
        tvScore2.setText("98 pts");

        Glide.with(Objects.requireNonNull(context))
                .load(R.drawable.sample_05)
                .apply(GlideOptions.getGlideOptions())
                .into(ivProfile3);
        tvName3.setText("Anthony");
        tvScore3.setText("96 pts");

    }

    private void loadRankings(String tab, String subjectId) {
        rankingsArrayList.clear();

        rankingsArrayList.add(new Rankings(R.drawable.sample_01,
                "Vanne",
                "Pagapong",
                "100",
                "A"));

        rankingsArrayList.add(new Rankings(R.drawable.sample_04,
                "Michael",
                "Ligdo",
                "90",
                "B"));

        rankingsArrayList.add(new Rankings(R.drawable.sample_05,
                "Earl",
                "Cuartero",
                "99",
                "C"));

        rankingsArrayList.add(new Rankings(R.drawable.sample_01,
                "Jasper",
                "Atillo",
                "97",
                "A"));

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        rankingsAdapter = new RankingsAdapter(context, rankingsArrayList);
        recyclerView.setAdapter(rankingsAdapter);
    }

    private void HideShowRanking(boolean show){
        cv1.setVisibility(show ? View.VISIBLE : View.GONE);
        cv2.setVisibility(show ? View.VISIBLE : View.GONE);
        cv3.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
        btnFilter.setVisibility(show ? View.VISIBLE : View.GONE);
        spSubjects.setVisibility(show ? View.VISIBLE : View.GONE);

        tvIndicator.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
