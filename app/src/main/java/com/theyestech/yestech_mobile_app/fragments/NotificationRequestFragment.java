package com.theyestech.yestech_mobile_app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.activities.ProfileSectionsActivity;
import com.theyestech.yestech_mobile_app.adapters.NotificationListAdapter;
import com.theyestech.yestech_mobile_app.adapters.NotififcationRequestAdapter;
import com.theyestech.yestech_mobile_app.adapters.SectionsAdapter;
import com.theyestech.yestech_mobile_app.adapters.StudentsAdapter;
import com.theyestech.yestech_mobile_app.adapters.SubjectRequestAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.NotificationList;
import com.theyestech.yestech_mobile_app.models.NotificationRequest;
import com.theyestech.yestech_mobile_app.models.Sections;
import com.theyestech.yestech_mobile_app.models.Students;
import com.theyestech.yestech_mobile_app.models.SubjectRequest;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class NotificationRequestFragment extends Fragment {

    private View view;
    private Context context;
    private String role;
    private RecyclerView recyclerView;
    private ArrayList<NotificationRequest> notificationRequests;
    private NotififcationRequestAdapter notififcationRequestAdapter;


    private ArrayList<SubjectRequest> studentsArrayList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private StudentsAdapter studentsAdapter;
    private SubjectRequestAdapter subjectRequestAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        role = getArguments().getString("ROLE");
        view = inflater.inflate(R.layout.fragment_notification_request, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
        if (role.equals(UserType.Educator())){
            getAllSubjectRequest();
        }
        else {
            showNotifications();
        }
//        //showNotifications();
    }

    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);


    }

    //Notification for Student
    private void showNotifications(){
        notificationRequests = new ArrayList<>();

        notificationRequests.add(new NotificationRequest(R.drawable.john1,"John Petrucci", "Bitok bitok"));
        notificationRequests.add(new NotificationRequest(R.drawable.soo_in,"Soo In Lee", "Pala Igit"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        notififcationRequestAdapter = new NotififcationRequestAdapter(getContext(), notificationRequests);

        recyclerView.setAdapter(notififcationRequestAdapter);
    }

    //Get All Subject Request From Student
    //Notification for Educator
    private void getAllSubjectRequest() {

        studentsArrayList.clear();

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("subj_id", UserSessionEducator.getID(context));

        //Wala pay API
        HttpProvider.defaultPost(context, "controller_educator/get_pending_students_subject.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("subj_id");
                        String ids = jsonObject.getString("stud_id");
                        String name = jsonObject.getString("stud_firstname");
                        String year = jsonObject.getString("stud_lastname");

                        SubjectRequest subjectRequest = new SubjectRequest(id,ids, name, year);

                        studentsArrayList.add(subjectRequest);
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setHasFixedSize(true);
                    subjectRequestAdapter = new SubjectRequestAdapter(getContext(), studentsArrayList);
                    recyclerView.setAdapter(subjectRequestAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.dismiss();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();

            }
        });
    }



}
