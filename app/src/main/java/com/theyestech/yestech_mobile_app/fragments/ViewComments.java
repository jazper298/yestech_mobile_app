package com.theyestech.yestech_mobile_app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.CommentAdapter;
import com.theyestech.yestech_mobile_app.models.Comments;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class ViewComments extends DialogFragment {
    private View view;
    private Context context;
    private ArrayList<Comments> commentlist;
    private CommentAdapter commentAdapter;
    private ListView listView;
    private RecyclerView recyclerView;
    private ImageView iv_CommentsBack;
    private ImageView iv_PostComment;
    private ProgressDialog progressDialog;

    public static ViewComments newInstance() {
        ViewComments f = new ViewComments();
        return f;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_comments,container,false);
        initializeUI();
        loadCommentsByTopic();
        return view;
    }
    private void initializeUI(){
        iv_CommentsBack = view.findViewById(R.id.iv_CommentsBack);
        iv_PostComment = view.findViewById(R.id.iv_PostComment);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

        iv_CommentsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().remove(ViewComments.this).commit();
            }
        });
        iv_PostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.success(getContext(), "Comments posted!").show();
                //postCommentByTopic();
            }
        });
//        commentlist = new ArrayList<>();
//        commentlist.add(new Comments(tc_id, R.drawable.kirk1, "Vanne Pagapong", tc_topic_id, tc_user_id, tc_details, tc_file, tc_datetime, user_image, user_fullname, "Comment namba 1"));
//        commentlist.add(new Comments(tc_id, R.drawable.james1, "Earl Kwartero", tc_topic_id, tc_user_id, tc_details, tc_file, tc_datetime, user_image, user_fullname, "Comment namba 2"));
//        commentlist.add(new Comments(tc_id, R.drawable.joe1, "Michael Jackson", tc_topic_id, tc_user_id, tc_details, tc_file, tc_datetime, user_image, user_fullname, "Comment namba 3"));
//        commentlist.add(new Comments(tc_id, R.drawable.john1, "Jasper Atillo", tc_topic_id, tc_user_id, tc_details, tc_file, tc_datetime, user_image, user_fullname, "Comment namba 4"));
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
//        recyclerView.setLayoutManager(rvLayoutManager);
//
//        commentAdapter = new CommentAdapter(getContext(), commentlist);
//
//        recyclerView.setAdapter(commentAdapter);
    }

    private void postCommentByTopic(int topic_id, String topic_comment) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("topic_id", topic_id);
        params.put("topic_comment", topic_comment);


        HttpProvider.postLogin(context, "controller_educator/post_comment_topic.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD(jsonObject.toString());
                    if (jsonArray.toString().contains("success")) {
                        Toasty.success(context, "Comment posted.").show();
                        loadCommentsByTopic();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }
    private void loadCommentsByTopic() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        commentlist.clear();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("topic_id", UserSessionEducator.getID(context));

        HttpProvider.defaultPost(context, "controller_educator/get_topic_comments.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.printO("SUBJECTS: " + jsonArray);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int tc_id = jsonObject.getInt("tc_id");
                        int  tc_topic_id = jsonObject.getInt("tc_topic_id");
                        int tc_user_id = jsonObject.getInt("tc_user_id");
                        String tc_details = jsonObject.getString("tc_details");
                        String tc_file = jsonObject.getString("tc_file");
                        String tc_datetime = jsonObject.getString("tc_datetime");
                        int user_image = jsonObject.getInt("user_image");
                        String user_fullname = jsonObject.getString("user_fullname");

                        Comments items = new Comments();
                        items.setTc_id(tc_id);
                        items.setTc_topic_id(tc_topic_id);
                        items.setTc_user_id(tc_user_id);
                        items.setTc_details(tc_details);
                        items.setTc_file(tc_file);
                        items.setTc_datetime(tc_datetime);
                        items.setUser_image(user_image);
                        items.setUser_fullname(user_fullname);

                        commentlist.add(items);
                    }
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                    recyclerView.setLayoutManager(rvLayoutManager);

                    commentAdapter = new CommentAdapter(getContext(), commentlist);

                    recyclerView.setAdapter(commentAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }
}
