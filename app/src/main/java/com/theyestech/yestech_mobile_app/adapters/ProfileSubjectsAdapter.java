package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Subjects;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProfileSubjectsAdapter extends RecyclerView.Adapter<ProfileSubjectsAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater layoutInflater;
    private List<Subjects> mList =  new ArrayList<>();
    private OnClickRecyclerView onClickRecyclerView;

    public ProfileSubjectsAdapter(@NonNull Context context, List<Subjects> list) {
        mContext = context;
        mList = list;
        layoutInflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public ProfileSubjectsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_profile_subjects,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileSubjectsAdapter.ViewHolder viewHolder, int i) {
        final Subjects subjectsList = mList.get(i);

        TextView tv_SubjectTitle = viewHolder.tv_SubjectTitle;

        Glide.with(mContext)
                .load(HttpProvider.getSubjectDir() + subjectsList.getSub_image())
                .placeholder(R.drawable.ic_subjects_accent)
                .into(viewHolder.imageView);

        tv_SubjectTitle.setText(subjectsList.getSub_title());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_SubjectTitle, tvIcon;
        private CardView cvSubjects;
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_SubjectTitle = itemView.findViewById(R.id.tv_SubjectTitle);
            imageView = itemView.findViewById(R.id.iv_SubjectImage);
            tvIcon = itemView.findViewById(R.id.tv_SubjectIcon);
            cvSubjects = itemView.findViewById(R.id.cv_ProfileSubjects);
            cvSubjects.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
