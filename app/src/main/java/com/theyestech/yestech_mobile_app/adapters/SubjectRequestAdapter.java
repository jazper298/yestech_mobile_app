package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Students;
import com.theyestech.yestech_mobile_app.models.SubjectRequest;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class SubjectRequestAdapter extends RecyclerView.Adapter<SubjectRequestAdapter.MyViewHolder>  {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<SubjectRequest> subjectRequestArrayList;
    private OnClickRecyclerView onClickRecyclerView;

    public SubjectRequestAdapter(Context context,ArrayList<SubjectRequest> subjectRequestArrayList) {
        this.context = context;
        this.subjectRequestArrayList = subjectRequestArrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_notification_request, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        SubjectRequest subjectRequest = subjectRequestArrayList.get(i);

        myViewHolder.tv_Username.setText(subjectRequest.getStud_firstname() + ", " + subjectRequest.getStud_lastname());

        myViewHolder.btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.info(context, "Confirmed!" ).show();
            }
        });

        myViewHolder.btn_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.error(context, "Deleted!" ).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjectRequestArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_Username;
        Button btn_Confirm,btn_Delete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_Username = itemView.findViewById(R.id.tv_Username);
            btn_Confirm = itemView.findViewById(R.id.btn_Confirm);
            btn_Delete = itemView.findViewById(R.id.btn_Delete);
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
