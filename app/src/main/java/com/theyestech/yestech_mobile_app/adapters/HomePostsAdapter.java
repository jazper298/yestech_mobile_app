package com.theyestech.yestech_mobile_app.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.fragments.ViewComments;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Posts;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;

import java.util.ArrayList;

public class HomePostsAdapter extends RecyclerView.Adapter<HomePostsAdapter.ViewHolder> {

    private Context mContext;
    private View view;
    private LayoutInflater layoutInflater;
    private ArrayList<Posts> postsArrayList = new ArrayList<>();
    private OnClickRecyclerView onClickRecyclerView;
    private BottomSheetBehavior mBehavior;

    FragmentManager fragmentManager;

    public HomePostsAdapter(Context context, ArrayList<Posts> list) {
        mContext = context;
        postsArrayList = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.listrow_home_posts, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        Posts posts = postsArrayList.get(i);

        viewHolder.tvFullname.setText(posts.getNf_fullname());
        viewHolder.tvDateTime.setText(posts.getNf_date());
        viewHolder.tvDetails.setText(posts.getNf_details());

        if (posts.getNf_files().isEmpty())
            viewHolder.ivImage.setVisibility(View.GONE);
        else
            Glide.with(mContext)
                    .load(HttpProvider.getNewsfeedDir() + posts.getNf_files())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            // log exception
                            Debugger.logD("GLIDERROR: " + e.toString());
                            return false; // important to return false so the error placeholder can be placed
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(viewHolder.ivImage);


        Glide.with(mContext)
                .load(HttpProvider.getProfileImageDir() + posts.getNf_image())
                .placeholder(R.drawable.ic_profile_accent)
                .apply(GlideOptions.getGlideOptions())
                .into(viewHolder.ivProfile);

        final ImageView ivComments = viewHolder.ivComments;

        ivComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //BottomSheetBehavior mBehavior;
                showCommentDialog();
                final Context context=v.getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View view = inflater.inflate(R.layout.fragment_view_comments, null);

                //ConstraintLayout views = (ConstraintLayout)view.findViewById(R.id.views);


                final Dialog mBottomSheetDialog = new Dialog (context);
                BottomSheetBehavior behavior = new BottomSheetBehavior();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                Window window = mBottomSheetDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                // BottomSheet

                mBottomSheetDialog.setContentView (view);
                mBottomSheetDialog.setCancelable (true);
                int width = ViewGroup.LayoutParams.MATCH_PARENT;
                int height = ViewGroup.LayoutParams.MATCH_PARENT;
                mBottomSheetDialog.getWindow ().setLayout (width,
                        height);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.CENTER);
                mBottomSheetDialog.show ();
//

            }
        });

        viewHolder.ivYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.ivYes.setImageResource(R.drawable.ic_yes_up);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something here
                        viewHolder.ivYes.setImageResource(R.drawable.ic_yes_clenched);
                    }
                }, 500);

            }
        });

        viewHolder.ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.ivFavorite.setImageResource(R.drawable.ic_favorite_accent);
            }
        });

    }
    private void showCommentDialog() {
        FragmentTransaction ft = ((AppCompatActivity)mContext).getSupportFragmentManager().beginTransaction();
        DialogFragment newFragment = ViewComments.newInstance();
        newFragment.setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        newFragment.show(ft, "dialog");

    }

    @Override
    public int getItemCount() {
        return postsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivProfile, ivImage, ivType, ivComments, ivYes, ivFavorite;
        private TextView tvFullname, tvDateTime, tvDetails;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfile = itemView.findViewById(R.id.iv_HomePostProfile);
            ivImage = itemView.findViewById(R.id.iv_HomePostImage);
            tvFullname = itemView.findViewById(R.id.tv_HomePostFullname);
            tvDateTime = itemView.findViewById(R.id.tv_HomePostDateTime);
            tvDetails = itemView.findViewById(R.id.tv_HomePostTitle);
            ivComments = itemView.findViewById(R.id.iv_Comments);
            ivYes = itemView.findViewById(R.id.iv_yes);
            ivFavorite = itemView.findViewById(R.id.iv_favorite);
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
