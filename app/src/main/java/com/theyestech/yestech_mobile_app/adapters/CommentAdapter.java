package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.models.Comments;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.Objects;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Comments> clist;
    public CommentAdapter(Context context, ArrayList<Comments> list){
        mContext = context;
        clist = list;
    }
    @NonNull
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_comment,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Comments comments = clist.get(i);
//        //ImageView ivCommentProfile = viewHolder.iv_CommentsProfile;
////
////        TextView tvFullNameComment, tvCommentContent;
////        tvFullNameComment = viewHolder.tv_FullNameComment;
////        tvCommentContent = viewHolder.tv_CommentContent;
//        viewHolder.tv_FullNameComment.setText(comments.getFullNameComments());
//        viewHolder.tv_CommentContent.setText(comments.getCommentsContent());
//
//
//        Glide.with(Objects.requireNonNull(mContext))
//                .load(comments.getCommentsProfile())
//                .apply(GlideOptions.getGlideOptions())
//                .into(viewHolder.iv_CommentsProfile);
    }



    @Override
    public int getItemCount() {
        return clist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_FullNameComment,tv_CommentContent ;
        ImageView iv_CommentsProfile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_CommentsProfile = (ImageView) itemView.findViewById(R.id.iv_CommentsProfile);
            tv_FullNameComment = (TextView) itemView.findViewById(R.id.tv_FullNameComment);
            tv_CommentContent = (TextView) itemView.findViewById(R.id.tv_CommentContent);

        }
    }
}
