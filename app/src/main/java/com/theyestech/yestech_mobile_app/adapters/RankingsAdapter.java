package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Rankings;
import com.theyestech.yestech_mobile_app.models.Subjects;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RankingsAdapter extends RecyclerView.Adapter<RankingsAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Rankings> rankingsArrayList =  new ArrayList<>();
    private OnClickRecyclerView onClickRecyclerView;

    public RankingsAdapter(@NonNull Context context, List<Rankings> list) {
        this.context = context;
        rankingsArrayList = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RankingsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.listrow_rankings,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RankingsAdapter.ViewHolder viewHolder, int i) {
        final Rankings rankings = rankingsArrayList.get(i);

        viewHolder.tvFullname.setText(rankings.getFirst_name() + " " + rankings.getLast_name());
        viewHolder.tvSection.setText(rankings.getSection());
        viewHolder.tvScore.setText(rankings.getScore() + " pts");

        Glide.with(Objects.requireNonNull(context))
                .load(rankings.getImage())
                .apply(GlideOptions.getGlideOptions())
                .into(viewHolder.ivProfile);
    }

    @Override
    public int getItemCount() {
        return rankingsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvFullname, tvScore, tvSection;
        ImageView ivProfile;
        ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvFullname = itemView.findViewById(R.id.tv_FullNameRankings);
            tvSection  =itemView.findViewById(R.id.tv_SectionRankings);
            tvScore = itemView.findViewById(R.id.tv_ScoreRankings);
            ivProfile = itemView.findViewById(R.id.iv_RankingsProfile);
            constraintLayout = itemView.findViewById(R.id.constraint_Rankings);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
}
