package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Students;

import java.util.ArrayList;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Students> studentsArrayList;
    private OnClickRecyclerView onClickRecyclerView;

    public StudentsAdapter(Context context, ArrayList<Students> studentsArrayList) {
        this.context = context;
        this.studentsArrayList = studentsArrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.listrow_profile_subject_students, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Students students = studentsArrayList.get(i);

        myViewHolder.tv_StudentName.setText(students.getStud_firstname() + ", " + students.getStud_lastname());

    }

    @Override
    public int getItemCount() {
        return studentsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_StudentName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_StudentName = itemView.findViewById(R.id.tv_StudentName);

        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
