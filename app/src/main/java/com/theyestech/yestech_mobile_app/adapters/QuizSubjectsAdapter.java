package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Subjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuizSubjectsAdapter extends RecyclerView.Adapter<QuizSubjectsAdapter.ViewHolder>{
    private Context mContext;
    private List<Subjects> mList =  new ArrayList<>();
    private OnClickRecyclerView onClickRecyclerView;

    public QuizSubjectsAdapter(@NonNull Context context, List<Subjects> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public QuizSubjectsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_quiz_subjects, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuizSubjectsAdapter.ViewHolder viewHolder, int i) {
        final Subjects subjectsList = mList.get(i);

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        TextView tv_SubjectTitle;
        TextView tvIcon;

        tvIcon = viewHolder.tvIcon;
        tv_SubjectTitle = viewHolder.tv_SubjectTitle;

        tv_SubjectTitle.setText(subjectsList.getSub_title());

        String icon = subjectsList.getSub_title().substring(0,1).trim();

        tvIcon.setText(icon);
        tvIcon.setBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_SubjectTitle, tvIcon;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_SubjectTitle = itemView.findViewById(R.id.tv_SubjectTitle);
            tvIcon = itemView.findViewById(R.id.tv_SubjectIcon);

            constraintLayout = itemView.findViewById(R.id.constraint_QuizSubject);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }

}
