package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.models.NotificationList;

import java.util.ArrayList;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder>  {

    private Context mContext;
    private ArrayList<NotificationList> mList;

    public NotificationListAdapter(Context context, ArrayList<NotificationList> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public NotificationListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_notification,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationListAdapter.ViewHolder viewHolder, int i) {
        final NotificationList notificationList = mList.get(i);
        ImageView iv_ProfilePicture = viewHolder.iv_ProfilePicture;
        ImageView iv_Icons = viewHolder.iv_Icons;
        ImageView iv_Post = viewHolder.iv_Post;
        ImageView iv_Icons2 = viewHolder.iv_Icons2;
        TextView tv_Username, tv_Description, tv_Hours;
        final RadioButton radioButton = viewHolder.radioButton;
        final RadioGroup radioGroup  = viewHolder.radioGroup;

        tv_Username = viewHolder.tv_Username;
        tv_Description = viewHolder.tv_Description;
        tv_Hours = viewHolder.tv_Hours;

        iv_ProfilePicture.setImageResource(notificationList.getIv_ProfilePicture());
        iv_Icons.setImageResource(notificationList.getIv_Icons());
        iv_Post.setImageResource(notificationList.getIv_Post());
        iv_Icons2.setImageResource(notificationList.getIv_Icons2());

        tv_Username.setText(notificationList.getTv_Username());
        tv_Description.setText(notificationList.getTv_Description());
        tv_Hours.setText(notificationList.getTv_Hours());
        radioButton.setChecked(true);
        iv_Icons2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEllipsesImportDialog();
            }
            //Show Notification Dialog
            private void showEllipsesImportDialog(){
                String items[] = {"Remove this notification", "Turn off notification about this post " };
                android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(mContext);

                dialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0){

                        }
                        if (which == 1){

                        }
                    }
                });
                dialog.create().show();
            }
        });
        viewHolder.radioGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButton.setChecked(false);
            }
        });

        viewHolder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case 0:
                        if(radioButton.isChecked())
                        {
                            radioButton.setChecked(false);
                        }

                        break;
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_ProfilePicture, iv_Icons, iv_Post, iv_Icons2;
        TextView tv_Username, tv_Description, tv_Hours;
        RadioButton radioButton;
        RadioGroup radioGroup;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_ProfilePicture = (ImageView) itemView.findViewById(R.id.iv_HomeImage);
            tv_Username = (TextView) itemView.findViewById(R.id.tv_Username);
            tv_Description = (TextView) itemView.findViewById(R.id.tv_Description);
            tv_Hours = (TextView) itemView.findViewById(R.id.tv_Hours);
            iv_Icons = (ImageView) itemView.findViewById(R.id.iv_Icons);
            iv_Post = (ImageView) itemView.findViewById(R.id.iv_Post);
            iv_Icons2 = (ImageView) itemView.findViewById(R.id.iv_Icons2);
            radioButton = (RadioButton) itemView.findViewById(R.id.radioButton);
            radioGroup = (RadioGroup) itemView.findViewById(R.id.radioGroup);
        }
    }



}
