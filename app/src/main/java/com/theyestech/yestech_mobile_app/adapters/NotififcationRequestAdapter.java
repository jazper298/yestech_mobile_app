package com.theyestech.yestech_mobile_app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.models.NotificationList;
import com.theyestech.yestech_mobile_app.models.NotificationRequest;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class NotififcationRequestAdapter extends RecyclerView.Adapter<NotififcationRequestAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<NotificationRequest> mList;

    public NotififcationRequestAdapter(Context context, ArrayList<NotificationRequest> list) {
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public NotififcationRequestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_notification_request,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotififcationRequestAdapter.ViewHolder viewHolder, int i) {
        final NotificationRequest notificationRequest = mList.get(i);
        ImageView iv_ProfilePicture = viewHolder.iv_ProfilePicture;
        TextView tv_Username, tv_Section;
        Button btn_Confirm, btn_Delete;

        tv_Username = viewHolder.tv_Username;
        tv_Section = viewHolder.tv_Section;
        btn_Confirm = viewHolder.btn_Confirm;
        btn_Delete = viewHolder.btn_Delete;

        iv_ProfilePicture.setImageResource(notificationRequest.getIv_ProfilePicture());
        tv_Username.setText(notificationRequest.getTv_Username());
        tv_Section.setText(notificationRequest.getTv_Section());

        btn_Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.success(mContext, "Confirm").show();
            }
        });

        btn_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.success(mContext, "Deleted").show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_ProfilePicture;
        TextView tv_Username, tv_Section;
        Button btn_Confirm, btn_Delete;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_ProfilePicture = (ImageView) itemView.findViewById(R.id.iv_HomeImage);
            tv_Username = (TextView) itemView.findViewById(R.id.tv_Username);
            tv_Section = (TextView) itemView.findViewById(R.id.tv_Section);
            btn_Confirm = (Button) itemView.findViewById(R.id.btn_Confirm);
            btn_Delete = (Button) itemView.findViewById(R.id.btn_Delete);

        }
    }
}
