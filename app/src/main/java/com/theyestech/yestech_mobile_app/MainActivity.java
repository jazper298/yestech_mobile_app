package com.theyestech.yestech_mobile_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.theyestech.yestech_mobile_app.activities.LoginActivity;
import com.theyestech.yestech_mobile_app.fragments.ChatFragment;
import com.theyestech.yestech_mobile_app.fragments.HomeFragment;
import com.theyestech.yestech_mobile_app.fragments.MenuFragment;
import com.theyestech.yestech_mobile_app.fragments.NotificationFragment;
import com.theyestech.yestech_mobile_app.fragments.QuizFragment;
import com.theyestech.yestech_mobile_app.fragments.RankingsFragment;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private BottomNavigationView navigation;
    private String role;

    private FragmentManager fragmentManager;

    private String currentFragment = "1";

    //Firebase
    FirebaseUser firebaseUser;
    DatabaseReference reference;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    currentFragment = "1";
                    openHomeFragment();
                    return true;
                case R.id.navigation_quiz:
                    currentFragment = "2";
                    openQuizFragment();
                    return true;
                case R.id.navigation_rankings:
                    currentFragment = "3";
                    openRankingsFragment();
                    return true;
                case R.id.navigation_notification:
                    currentFragment = "4";
                    openNotificationFragment();
                    return true;
                case R.id.navigation_menu:
                    currentFragment = "5";
                    openMenuFragment();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        context = this;
        checkUserSession();
//        checkUserFirebaseSession();
    }


    @Override
    public void onBackPressed() {
        if (!currentFragment.equals("1")) {
            currentFragment = "1";
            openHomeFragment();
            navigation.setSelectedItemId(R.id.navigation_home);
        } else {
            super.onBackPressed();
            finish();
        }

    }

    private void openHomeFragment() {
        setTitle("Home");
        HomeFragment homeFragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, homeFragment, homeFragment.getTag()).commit();

        Debugger.logD("FBTOKEN : " + UserSessionEducator.getFirebaseToken(context));
    }

    private void openChatFragment() {
        setTitle("Chat");
        ChatFragment chatFragment = new ChatFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, chatFragment, chatFragment.getTag()).commit();
    }

    private void openQuizFragment() {
        setTitle("Quiz");
        QuizFragment quizFragment = new QuizFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, quizFragment, quizFragment.getTag()).commit();
    }
    private void openRankingsFragment() {
        setTitle("Rankings");
        RankingsFragment rankingsFragment = new RankingsFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, rankingsFragment, rankingsFragment.getTag()).commit();
    }
    private void openNotificationFragment() {
        setTitle("Notification");
        NotificationFragment notificationFragment = new NotificationFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, notificationFragment, notificationFragment.getTag()).commit();
    }

    private void openMenuFragment() {
        setTitle("Menu");
        MenuFragment menuFragment = new MenuFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, menuFragment, menuFragment.getTag()).commit();
    }

    public void checkUserSession() {

        String studToken = UserSessionStudent.getToken(context);
        String teachToken = UserSessionEducator.getToken(context);

        if (studToken.equals("") && teachToken.equals("") && FirebaseAuth.getInstance().getCurrentUser() == null) {
            Intent login = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(login);
            finish();
        } else {
            if (!studToken.equals("") && !UserSessionStudent.getFirebaseToken(context).equals("")) {
                UserType userType = new UserType();
                userType.setUserRole(UserType.Student());
                userType.saveRole(context);

                openHomeFragment();
            }
            if (!teachToken.equals("") && !UserSessionEducator.getFirebaseToken(context).equals("")) {
                UserType userType = new UserType();
                userType.setUserRole(UserType.Educator());
                userType.saveRole(context);

                openHomeFragment();
            }
        }
    }

}
