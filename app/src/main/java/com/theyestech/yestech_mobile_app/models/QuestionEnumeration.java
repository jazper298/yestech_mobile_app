package com.theyestech.yestech_mobile_app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionEnumeration implements Parcelable {

    private int id;
    private String question;
    private String option1;
    private String answerNr;
    private int categoryID;

    public QuestionEnumeration() {
    }

    public QuestionEnumeration(String question, String option1,
                               String answerNr, int categoryID) {
        this.question = question;
        this.option1 = option1;
        this.answerNr = answerNr;
        this.categoryID = categoryID;
    }

    protected QuestionEnumeration(Parcel in) {
        id = in.readInt();
        question = in.readString();
        option1 = in.readString();
        answerNr = in.readString();
        categoryID = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(question);
        dest.writeString(option1);
        dest.writeString(answerNr);
        dest.writeInt(categoryID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionEnumeration> CREATOR = new Creator<QuestionEnumeration>() {
        @Override
        public QuestionEnumeration createFromParcel(Parcel in) {
            return new QuestionEnumeration(in);
        }

        @Override
        public QuestionEnumeration[] newArray(int size) {
            return new QuestionEnumeration[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getAnswerNr() {
        return answerNr;
    }

    public void setAnswerNr(String answerNr) {
        this.answerNr = answerNr;
    }


    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

}
