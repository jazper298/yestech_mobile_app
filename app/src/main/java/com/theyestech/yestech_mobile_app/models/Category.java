package com.theyestech.yestech_mobile_app.models;

public class Category {
    public static final int MULTIPLECHOICE = 1;
    public static final int TRUEORFALSE = 2;
    public static final int ENUMERATION = 3;

    private int id;
    private String name;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}