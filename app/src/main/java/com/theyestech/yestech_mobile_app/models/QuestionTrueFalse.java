package com.theyestech.yestech_mobile_app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class QuestionTrueFalse implements Parcelable {
    private int id;
    private String question;
    private String option1;
    private String option2;
    private int answerNr;
    private int categoryID;

    public QuestionTrueFalse() {
    }

    public QuestionTrueFalse(String question, String option1, String option2,
                    int answerNr, int categoryID) {
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.answerNr = answerNr;
        this.categoryID = categoryID;
    }

    protected QuestionTrueFalse(Parcel in) {
        id = in.readInt();
        question = in.readString();
        option1 = in.readString();
        option2 = in.readString();
        answerNr = in.readInt();
        categoryID = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(question);
        dest.writeString(option1);
        dest.writeString(option2);
        dest.writeInt(answerNr);
        dest.writeInt(categoryID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionTrueFalse> CREATOR = new Creator<QuestionTrueFalse>() {
        @Override
        public QuestionTrueFalse createFromParcel(Parcel in) {
            return new QuestionTrueFalse(in);
        }

        @Override
        public QuestionTrueFalse[] newArray(int size) {
            return new QuestionTrueFalse[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public int getAnswerNr() {
        return answerNr;
    }

    public void setAnswerNr(int answerNr) {
        this.answerNr = answerNr;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

}
