package com.theyestech.yestech_mobile_app.models;

public class Comments {
    private int tc_id;
//    private int commentsProfile;
//    private String fullNameComments;
    private int tc_topic_id;
    private int tc_user_id;
    private String tc_details;
    private String tc_file;
    private String tc_datetime;
    private int user_image;
    private String user_fullname;

    //public int getCommentsProfile() {
     //   return commentsProfile;
   // }

    //public void setCommentsProfile(int commentsProfile) {
      //  this.commentsProfile = commentsProfile;
    //}

   // public String getFullNameComments() {
      //  return fullNameComments;
    //}

   // public void setFullNameComments(String fullNameComments) {
       // this.fullNameComments = fullNameComments;
    //}

    //public String getCommentsContent() {
        //return commentsContent;
    //}

   // public void setCommentsContent(String commentsContent) {this.commentsContent = commentsContent;
    //}

    public Comments() {
        
    }

    public Comments(int tc_id, int tc_topic_id, int tc_user_id, String tc_details, String tc_file, String tc_datetime, int user_image, String user_fullname) {
        this.tc_id = tc_id;
        //this.commentsProfile = commentsProfile;
        //this.fullNameComments = fullNameComments;
        this.tc_topic_id = tc_topic_id;
        this.tc_user_id = tc_user_id;
        this.tc_details = tc_details;
        this.tc_file = tc_file;
        this.tc_datetime = tc_datetime;
        this.user_image = user_image;
        this.user_fullname = user_fullname;
        //this.commentsContent = commentsContent;
    }

    //private String commentsContent;


    public int getTc_id() {
        return tc_id;
    }

    public void setTc_id(int tc_id) {
        this.tc_id = tc_id;
    }

    public int getTc_topic_id() {
        return tc_topic_id;
    }

    public void setTc_topic_id(int tc_topic_id) {
        this.tc_topic_id = tc_topic_id;
    }

    public int getTc_user_id() {
        return tc_user_id;
    }

    public void setTc_user_id(int tc_user_id) {
        this.tc_user_id = tc_user_id;
    }

    public String getTc_details() {
        return tc_details;
    }

    public void setTc_details(String tc_details) {
        this.tc_details = tc_details;
    }

    public String getTc_file() {
        return tc_file;
    }

    public void setTc_file(String tc_file) {
        this.tc_file = tc_file;
    }

    public String getTc_datetime() {
        return tc_datetime;
    }

    public void setTc_datetime(String tc_datetime) {
        this.tc_datetime = tc_datetime;
    }

    public int getUser_image() {
        return user_image;
    }

    public void setUser_image(int user_image) {
        this.user_image = user_image;
    }

    public String getUser_fullname() {
        return user_fullname;
    }

    public void setUser_fullname(String user_fullname) {
        this.user_fullname = user_fullname;
    }
}
