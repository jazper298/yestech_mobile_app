package com.theyestech.yestech_mobile_app.models;

public class NotificationList {
    private int iv_ProfilePicture, iv_Icons, iv_Post, iv_Icons2;
    private String tv_Username, tv_Description, tv_Hours;

    public NotificationList(int iv_ProfilePicture, int iv_Icons, int iv_Post, int iv_Icons2, String tv_Username, String tv_Description, String tv_Hours){
        this.iv_ProfilePicture = iv_ProfilePicture;
        this.iv_Icons = iv_Icons;
        this.iv_Post = iv_Post;
        this.iv_Icons2 = iv_Icons2;

        this.tv_Username = tv_Username;
        this.tv_Description = tv_Description;
        this.tv_Hours = tv_Hours;

    }

    public int getIv_ProfilePicture() {
        return iv_ProfilePicture;
    }

    public int getIv_Icons() {
        return iv_Icons;
    }

    public int getIv_Post() {
        return iv_Post;
    }

    public int getIv_Icons2() {
        return iv_Icons2;
    }


    public String getTv_Username() {
        return tv_Username;
    }

    public String getTv_Description() {
        return tv_Description;
    }

    public String getTv_Hours() {
        return tv_Hours;
    }


    public void setIv_ProfilePicture(int iv_ProfilePicture) {
        this.iv_ProfilePicture = iv_ProfilePicture;
    }

    public void setIv_Icons(int iv_Icons) {
        this.iv_Icons = iv_Icons;
    }

    public void setIv_Post(int iv_Post) {
        this.iv_Post = iv_Post;
    }

    public void setIv_Icons2(int iv_Icons2) {
        this.iv_Icons2 = iv_Icons2;
    }

    public void setTv_Username(String tv_Username) {
        this.tv_Username = tv_Username;
    }

    public void setTv_Description(String tv_Description) {
        this.tv_Description = tv_Description;
    }

    public void setTv_Hours(String tv_Hours) {
        this.tv_Hours = tv_Hours;
    }
}
