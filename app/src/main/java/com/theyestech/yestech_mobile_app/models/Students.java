package com.theyestech.yestech_mobile_app.models;

import com.google.gson.annotations.SerializedName;

public class Students {

    @SerializedName("stud_id")
    private String stud_id;
    @SerializedName("stud_firstname")
    private String stud_firstname;
    @SerializedName("stud_lastname")
    private String stud_lastname;

    public Students(String stud_id, String stud_firstname, String stud_lastname) {
        this.stud_id = stud_id;
        this.stud_firstname = stud_firstname;
        this.stud_lastname = stud_lastname;
    }

    public Students() {

    }

    public String getStud_id() {
        return stud_id;
    }

    public void setStud_id(String stud_id) {
        this.stud_id = stud_id;
    }

    public String getStud_firstname() {
        return stud_firstname;
    }

    public void setStud_firstname(String stud_firstname) {
        this.stud_firstname = stud_firstname;
    }

    public String getStud_lastname() {
        return stud_lastname;
    }

    public void setStud_lastname(String stud_lastname) {
        this.stud_lastname = stud_lastname;
    }
}
