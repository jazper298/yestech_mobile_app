package com.theyestech.yestech_mobile_app.models;

public class NotificationRequest {

    private int iv_ProfilePicture;//, btn_Confirm, btn_Delete;
    private String tv_Username, tv_Section;

    public NotificationRequest(int iv_ProfilePicture, String tv_Username, String tv_Section){
        this.iv_ProfilePicture = iv_ProfilePicture;
//        this.btn_Confirm = btn_Confirm;
//        this.btn_Delete = btn_Delete;

        this.tv_Username = tv_Username;
        this.tv_Section = tv_Section;

    }

    public int getIv_ProfilePicture() {
        return iv_ProfilePicture;
    }

    public void setIv_ProfilePicture(int iv_ProfilePicture) {
        this.iv_ProfilePicture = iv_ProfilePicture;
    }

//    public int getBtn_Confirm() {
//        return btn_Confirm;
//    }
//
//    public void setBtn_Confirm(int btn_Confirm) {
//        this.btn_Confirm = btn_Confirm;
//    }
//
//    public int getBtn_Delete() {
//        return btn_Delete;
//    }
//
//    public void setBtn_Delete(int btn_Delete) {
//        this.btn_Delete = btn_Delete;
//    }

    public String getTv_Username() {
        return tv_Username;
    }

    public void setTv_Username(String tv_Username) {
        this.tv_Username = tv_Username;
    }

    public String getTv_Section() {
        return tv_Section;
    }

    public void setTv_Section(String tv_Section) {
        this.tv_Section = tv_Section;
    }
}
