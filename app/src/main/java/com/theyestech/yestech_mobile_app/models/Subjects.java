package com.theyestech.yestech_mobile_app.models;

public class Subjects {

    private String subj_id;
    private String subj_title;
    private String sub_description;
    private String sub_image;

    public String getSubject_id() {
        return subj_id;
    }

    public void setSubject_id(String subj_id) {
        this.subj_id = subj_id;
    }

    public String getSub_title() {
        return subj_title;
    }

    public void setSub_title(String subj_title) {
        this.subj_title = subj_title;
    }

    public String getSub_description() {
        return sub_description;
    }

    public void setSection_year(String sub_description) {
        this.sub_description = sub_description;
    }

    public String getSub_image() {
        return sub_image;
    }

    public void setSub_image(String sub_image) {
        this.sub_image = sub_image;
    }
}
