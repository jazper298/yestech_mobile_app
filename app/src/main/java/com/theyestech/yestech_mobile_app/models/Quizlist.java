package com.theyestech.yestech_mobile_app.models;



public class Quizlist {

    private String quiz_id;
    private String subject_id;
    private String quiz_title;
    private String quiz_type;
    private String quiz_item;
    private String quiz_time;

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getQuiz_title() {
        return quiz_title;
    }

    public void setQuiz_title(String quiz_title) {
        this.quiz_title = quiz_title;
    }

    public String getQuiz_type() {
        return quiz_type;
    }

    public void setQuiz_type(String quiz_type) {
        this.quiz_type = quiz_type;
    }

    public String getQuiz_item() {
        return quiz_item;
    }

    public void setQuiz_item(String quiz_item) {
        this.quiz_item = quiz_item;
    }

    public String getQuiz_time() {
        return quiz_time;
    }

    public void setQuiz_time(String quiz_time) {
        this.quiz_time = quiz_time;
    }
}
