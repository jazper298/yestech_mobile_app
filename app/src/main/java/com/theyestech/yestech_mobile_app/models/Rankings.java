package com.theyestech.yestech_mobile_app.models;

public class Rankings {

    private String id;

    private int image;

    private String first_name;

    private String last_name;

    private String score;

    private String section;

    public Rankings(){

    }

    public Rankings(int image, String first_name, String last_name, String score, String section) {
        this.image = image;
        this.first_name = first_name;
        this.last_name = last_name;
        this.score = score;
        this.section = section;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
