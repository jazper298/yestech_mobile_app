package com.theyestech.yestech_mobile_app.models;

public class Sections {

    private String sec_id;
    private String sec_name;
    private String sec_year;

    public Sections(String sec_id, String sec_name, String sec_year) {
        this.sec_id = sec_id;
        this.sec_name = sec_name;
        this.sec_year = sec_year;
    }

    public String getSec_id() {
        return sec_id;
    }

    public void setSec_id(String sec_id) {
        this.sec_id = sec_id;
    }

    public String getSec_name() {
        return sec_name;
    }

    public void setSec_name(String sec_name) {
        this.sec_name = sec_name;
    }

    public String getSec_year() {
        return sec_year;
    }

    public void setSec_year(String sec_year) {
        this.sec_year = sec_year;
    }

}
