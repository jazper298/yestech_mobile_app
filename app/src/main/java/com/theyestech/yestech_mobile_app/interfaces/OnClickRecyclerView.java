package com.theyestech.yestech_mobile_app.interfaces;

import android.view.View;

public interface OnClickRecyclerView {

        void onItemClick(View view, int position);
}
