package com.theyestech.yestech_mobile_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.MainActivity;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.LoginPageAdapter;
import com.theyestech.yestech_mobile_app.fragments.LoginStudentFragment;
import com.theyestech.yestech_mobile_app.fragments.LoginTeacherFragment;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private ViewPager viewPager;
    private LoginPageAdapter loginPageAdapter;
    private TabLayout tabLayout;

    //Widgets
//    private TextView tv_CreateAccount;
//    private Button btn_Next;
//    private Spinner spLogin;
//    private EditText etEmail, etPassword;
//    private ProgressBar progressBar;
//    private View loginView;
//
//    private String selected;


    //Firebase
    FirebaseUser firebaseUser;

    @Override
    protected void onStart() {
        super.onStart();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        //Firebase Database
        //check if user is null
//        if (firebaseUser != null){
//            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//            startActivity(intent);
//            finish();
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        view = LoginActivity.this.getCurrentFocus();

        initializeUI();

        KeyboardHandler.hideKeyboard(LoginActivity.this);
    }

    private void initializeUI() {
        tabLayout = findViewById(R.id.tabLayoutLogin);
        viewPager = findViewById(R.id.vpLogin);
        loginPageAdapter = new LoginPageAdapter(getSupportFragmentManager());
        loginPageAdapter.addFragment(LoginStudentFragment.newInstance(), "S t u d e n t");
        loginPageAdapter.addFragment(LoginTeacherFragment.newInstance(), "E d u c a t o r");

        viewPager.setAdapter(loginPageAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

}
