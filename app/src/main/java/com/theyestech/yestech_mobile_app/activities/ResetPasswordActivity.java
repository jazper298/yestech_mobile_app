package com.theyestech.yestech_mobile_app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class ResetPasswordActivity extends AppCompatActivity {
    private Context context;
    private View view;

    private UserLoginTask mAuthTask = null;
    //Widgets
    private ImageView iv_AppLogo;
    private TextView tv_AppName;
    private TextView tv_ForgotPassword;
    private Button btn_Reset;
    private View mProgressView;
    private View mLoginFormView;
    private TextInputEditText input_Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        context = this;
        view = ResetPasswordActivity.this.getCurrentFocus();

    }
    @Override
    protected void onStart() {
        super.onStart();

        KeyboardHandler.hideKeyboard(ResetPasswordActivity.this);

        initializeUI();
    }

    private void initializeUI(){
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        iv_AppLogo = findViewById(R.id.iv_AppLogo);
        tv_AppName = findViewById(R.id.tv_AppName);
        tv_ForgotPassword = findViewById(R.id.tv_ForgotPassword);
        input_Email = (TextInputEditText)findViewById(R.id.input_Email);

        btn_Reset = findViewById(R.id.btn_Reset);

        btn_Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                //Toasty.success(getApplicationContext(), "ATIK RA!").show();
                startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
            }
        });
    }
    private void validateEmail()
    {

        try
        {
            if (mAuthTask != null) {
                return;
            }

            // Reset errors.
            input_Email.setError(null);

            // Store values at the time of the login attempt.
            String email = input_Email.getText().toString();

            boolean cancel = false;
            View focusView = null;

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);

                JSONObject jsonParams = new JSONObject();

                jsonParams.put("username", email);

                StringEntity entity = new StringEntity(jsonParams.toString());
                //doLogin(entity);

            }

        } catch (Exception err)
        {
            Toasty.error(getApplicationContext(), err.toString()).show();
        }
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;

        UserLoginTask(String email, String password) {
            mEmail = email;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mEmail)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
    //Load Progress Bar
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
