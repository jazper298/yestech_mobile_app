package com.theyestech.yestech_mobile_app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.QuizListAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Quizlist;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class QuizListActivity extends AppCompatActivity {

    private Context context;
    private View view;
    private ImageView iv_QuizListBack;
    private ImageView iv_QuizListAdd;
    private EditText et_QuizTitleSearch;
    private RecyclerView rv_QuizList;

    private ArrayList<Quizlist> quizlistArrayList;
    private QuizListAdapter quizListAdapter;
    private Quizlist selected_quiz;

    private ProgressDialog progressDialog;

    private String quizType;
    private String quizItems;

    private String role;
    private String subjectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        KeyboardHandler.hideKeyboard(this);

        Bundle bundle = getIntent().getExtras();
        subjectId = bundle.getString("SUBJECT_ID");

        role = UserType.getRole(context);

        initializeUI();
    }

    private void initializeUI() {
        iv_QuizListBack = findViewById(R.id.iv_QuizListBack);
        iv_QuizListAdd = findViewById(R.id.iv_QuizListAdd);
        et_QuizTitleSearch = findViewById(R.id.et_QuizTitleSearch);
        rv_QuizList = findViewById(R.id.rv_QuizList);

        if (role.equals(UserType.Student())) {
            iv_QuizListAdd.setVisibility(View.GONE);
            showQuizListStudent();
        } else {
            showQuizListEducator();
        }

        iv_QuizListBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_QuizListAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddQuiz();
            }
        });
    }

    private void showQuizListEducator() {

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please wait...");
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("subj_id", subjectId);

        HttpProvider.defaultPost(context, "controller_educator/get_quizzes_subject.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD("QUIZLIST: " + jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String quiz_id = jsonObject.getString("quiz_id");
                        String subject_id = jsonObject.getString("subject_id");
                        String quiz_title = jsonObject.getString("quiz_title");
                        String quiz_type = jsonObject.getString("quiz_type");
                        String quiz_item = jsonObject.getString("quiz_item");
                        String quiz_time = jsonObject.getString("quiz_time");

                        Quizlist quizlist = new Quizlist();
                        quizlist.setQuiz_id(quiz_id);
                        quizlist.setQuiz_item(quiz_item);
                        quizlist.setQuiz_time(quiz_time);
                        quizlist.setQuiz_title(quiz_title);
                        quizlist.setQuiz_type(quiz_type);
                        quizlist.setSubject_id(subject_id);
                        quizlistArrayList.add(quizlist);
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                    rv_QuizList.setLayoutManager(rvLayoutManager);
                    rv_QuizList.setLayoutManager(new GridLayoutManager(context, 2));

                    quizListAdapter = new QuizListAdapter(context, quizlistArrayList);
                    quizListAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selected_quiz = quizlistArrayList.get(position);

                        }
                    });

                    rv_QuizList.setAdapter(quizListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void showQuizListStudent() {

    }

    private void openAddQuiz() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_create_quiz, null);
        final Button btn_CreateQuiz;
        final EditText et_CreateQuizTitle;
        final Spinner sp_SelectLesson, sp_QuizType, sp_QuizItems;

        et_CreateQuizTitle = dialogView.findViewById(R.id.et_CreateQuizTitle);
        btn_CreateQuiz = dialogView.findViewById(R.id.btn_CreateQuiz);
        sp_SelectLesson = dialogView.findViewById(R.id.sp_SelectLesson);
        sp_QuizType = dialogView.findViewById(R.id.sp_QuizType);
        sp_QuizItems = dialogView.findViewById(R.id.sp_QuizItems);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        sp_QuizType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //final String quizType;
                switch (position) {
                    case 0:
                        quizType = "Nothing Selected";
                        break;
                    case 1:
                        quizType = "Multiple Choice";
                        break;
                    case 2:
                        quizType = "Enumeration";
                        break;
                    case 3:
                        quizType = "True or False";
                        break;
                    case 4:
                        quizType = "Matching Type";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toasty.warning(context, "Please Select Quiz Type").show();
            }

        });
        sp_QuizItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //final String quizType;
                switch (position) {
                    case 0:
                        quizItems = "0";
                        break;
                    case 1:
                        quizItems = "5";
                        break;
                    case 2:
                        quizItems = "10";
                        break;
                    case 3:
                        quizItems = "15";
                        break;
                    case 4:
                        quizItems = "20";
                        break;
                    case 5:
                        quizItems = "25";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toasty.warning(context, "Please Select Quiz Items").show();
            }

        });

        btn_CreateQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String quizTitle = et_CreateQuizTitle.getText().toString();

                if (quizType == "Nothing Selected") {
                    Toasty.warning(context, "Please Select Quiz Type").show();
                } else if (quizItems == "0") {
                    Toasty.warning(context, "Please Select Quiz Items").show();
                } else {
                    saveQuiz(subjectId, quizTitle, quizType, quizItems, b);
                }
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void saveQuiz(String subjectId, String quizTitle, String quizType, String quizItem, AlertDialog b) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("subj_id", subjectId);
        params.put("quiz_title", quizTitle);
        params.put("quiz_type", quizType);
        params.put("quiz_item", quizItem);
        params.put("quiz_time", "");
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/create_quiz.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String response = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    Debugger.logD("QUIZ SAVED:" + response);

                    String quiz_id = jsonObject.getString("quiz_id");

                    Intent intent = new Intent(context, QuizActivity.class);
                    intent.putExtra("QUIZ_ID", quiz_id);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.printError(e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }
}
