package com.theyestech.yestech_mobile_app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.theyestech.yestech_mobile_app.R;

public class MenuNotesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
    }
}
