package com.theyestech.yestech_mobile_app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.StudentsAdapter;
import com.theyestech.yestech_mobile_app.models.Students;
import com.theyestech.yestech_mobile_app.models.Subjects;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static android.view.View.GONE;

public class SubjectDescActivity extends AppCompatActivity {

    private Context context;
    private String subject_id;
    private int position;

    private TextView tvSubject;
    private TextView tv_SubjectIcon;
    private ImageView iv_SectionBack, iv_Add;
    private EditText et_SubjectStudentSearch;
    private RecyclerView rv_StudentSubject;
    private Button btnDetails;

    private ArrayList<Subjects> subjectsArrayList = new ArrayList<>();
    private Subjects selected_subjects;

    private ArrayList<Students> studentsArrayList = new ArrayList<>();
    private Students students;
    private StudentsAdapter studentsAdapter;

    private ProgressDialog progressDialog;
    private ArrayList<String> subject_ids, subject_names;
    private ArrayList<String> section_ids, section_names;
    private String selected_level, selected_semester, selected_section;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_desc);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        subject_id = bundle.getString("SUBJECT_ID");

        context = this;

        KeyboardHandler.hideKeyboard(SubjectDescActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        subject_ids = new ArrayList<>();
        subject_names = new ArrayList<>();
        section_ids = new ArrayList<>();
        section_names= new ArrayList<>();

//        progressDialog.hide();

        initializeUI();
//        loadStudentsToSubject();
//        searchStudentsFromSubject();
    }

    private void initializeUI(){
        tv_SubjectIcon = findViewById(R.id.tv_SubjectIcon);
        iv_SectionBack = findViewById(R.id.iv_SectionBack);
        iv_Add = findViewById(R.id.iv_ProfileEditSubjectAdd);
        et_SubjectStudentSearch = findViewById(R.id.et_SubjectStudentSearch);

        btnDetails = findViewById(R.id.btn_ProfileSubjectDetails);

        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addForEducator();
            }
        });

        iv_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addForStudent(v);
            }
        });

        iv_SectionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void addForStudent(View v) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_join_subject, null);
        final EditText etCode;
        final Button btnSend;

        etCode = dialogView.findViewById(R.id.et_JoinSubjectCode);
        btnSend = dialogView.findViewById(R.id.btn_JoinSubjectSend);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etCode.getText().toString();

                if (code.isEmpty()) {
                    Toasty.warning(context, "Please input subject code.").show();
                } else {
                    sendCode(b, code);
                }
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void sendCode(final AlertDialog b, String code) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("stud_code", code);
        params.put("subj_id", subject_id);
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/add_student_to_subject.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                b.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Toasty.success(context, "Request sent.").show();
                    b.hide();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void loadStudentsToSubject(){
        studentsArrayList.clear();
        RequestParams params = new RequestParams();
        params.put("subj_id", subject_id);
        params.put("teach_token", UserSessionEducator.getID(context));
        HttpProvider.defaultPost(context, "controller_educator/get_students_from_subject.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String stud_id = jsonObject.getString("stud_id");
                        String stud_firstname = jsonObject.getString("stud_firstname");
                        String stud_lastname = jsonObject.getString("stud_lastname");

                        Students items = new Students();
                        items.setStud_id(stud_id);
                        items.setStud_firstname(stud_firstname);
                        items.setStud_lastname(stud_lastname);
                        studentsArrayList.add(items);
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    RecyclerView.LayoutManager rvLayoutManager = layoutManager;
                    rv_StudentSubject.setLayoutManager(rvLayoutManager);

                    studentsAdapter = new StudentsAdapter(context, studentsArrayList);

                    rv_StudentSubject.setAdapter(studentsAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });

    }

    private void searchStudentsFromSubject(){

    }

    private void addForEducator() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_subject, null);
        final EditText etTitle, etDescription;
        final Spinner spSection, spLevel, spSemester;
        final TextView tvSemester, tvSchoolYear, tvDialogLabel;
        final Button btnSave;

        etTitle = dialogView.findViewById(R.id.subj_title);
        etDescription = dialogView.findViewById(R.id.subj_desc);
        spSection = dialogView.findViewById(R.id.sp_AddSubjectSection);
        spLevel = dialogView.findViewById(R.id.sp_AddSubjectLevel);
        spSemester = dialogView.findViewById(R.id.sp_AddSubjectSemester);
        tvSemester = dialogView.findViewById(R.id.tv_AddSubjectSemester);
        tvSchoolYear = dialogView.findViewById(R.id.tv_AddSubjectSchoolYear);
        tvDialogLabel = dialogView.findViewById(R.id.tv_SubjectDialog);
        btnSave = dialogView.findViewById(R.id.btn_AddSubjectSave);

        tvDialogLabel.setText("Edit Subject");

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.level, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLevel.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(context, R.array.semester, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSemester.setAdapter(adapter1);

        spSection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_section = section_ids.get(spSection.getSelectedItemPosition());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        getSections(params, spSection);

        RequestParams params1 = new RequestParams();
        params1.put("subj_id", subject_id);
        params1.put("teach_id", UserSessionEducator.getID(context));
        params1.put("teach_token", UserSessionEducator.getToken(context));
        getSubjectDetails(params1, etTitle, etDescription, spSection, spLevel, spSemester, adapter, adapter1);


        spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    selected_level = "None";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                } else if (id == 1) {
                    selected_level = "Primary";
                    spSemester.setVisibility(GONE);
                    tvSemester.setVisibility(GONE);
                } else if (id == 2) {
                    selected_level = "Secondary";
                    spSemester.setVisibility(GONE);
                    tvSemester.setVisibility(GONE);
                } else if (id == 3) {
                    selected_level = "Tertiary";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    selected_semester = "";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                } else if (id == 1) {
                    selected_semester = "1st";
                } else if (id == 2) {
                    selected_semester = "2nd";
                } else if (id == 3) {
                    selected_semester = "Summer";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        Calendar calendar = Calendar.getInstance();

        final String year = (calendar.get(Calendar.YEAR) - 1) + " - " + calendar.get(Calendar.YEAR);
        tvSchoolYear.setText(year);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString();
                String desc = etDescription.getText().toString();

                if (title.isEmpty()) {
                    Toasty.warning(context, "Please input subject title.").show();
                } else {
                    if (selected_level.equals("Primary") || selected_level.equals("Secondary")) {
                        updateSubject(title, desc, selected_level, selected_section, selected_semester, year);
                        b.hide();
                    } else if (selected_level.equals("Tertiary")) {
                        if (selected_semester.equals("")) {
                            Toasty.warning(context, "Please select semester.").show();
                        } else {
                            updateSubject(title, desc, selected_level, selected_section, selected_semester, year);
                            b.hide();
                        }
                    } else {
                        Toasty.warning(context, "Please select level.").show();
                    }
                }
            }
        });

        b.show();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void getSections(RequestParams params, final Spinner spSection) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setTitle("Please wait");
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        section_ids.clear();
        section_names.clear();

        HttpProvider.defaultPost(context, "controller_educator/get_sections.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        section_ids.add(jsonObject.getString("section_id"));
                        section_names.add(jsonObject.getString("section_name"));
                    }

                    ArrayAdapter<String> sectionAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, section_names);
                    spSection.setAdapter(sectionAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void getSubjectDetails(RequestParams params, final EditText ettitle, final EditText etdesc, final Spinner spsection, final Spinner splevel, final Spinner spsemeseter, final ArrayAdapter<CharSequence> leveladapter, final ArrayAdapter<CharSequence> semesteradapter) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setTitle("Please wait");
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        HttpProvider.defaultPost(context, "controller_educator/get_subject_details.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    String title = jsonArray.getJSONObject(0).getString("subj_title");
                    String desc = jsonArray.getJSONObject(0).getString("subj_description");
                    final String section = jsonArray.getJSONObject(0).getString("section_id");
                    final String level = jsonArray.getJSONObject(0).getString("subj_level");
                    final String semester = jsonArray.getJSONObject(0).getString("subj_semester");

                    ettitle.setText(title);
                    etdesc.setText(desc);
                    spsection.post(new Runnable() {
                        @Override
                        public void run() {
                            spsection.setSelection(section_ids.indexOf(section));
                        }
                    });
                    splevel.post(new Runnable() {
                        @Override
                        public void run() {
                            splevel.setSelection(leveladapter.getPosition(level));
                        }
                    });
                    spsemeseter.post(new Runnable() {
                        @Override
                        public void run() {
                            spsemeseter.setSelection(semesteradapter.getPosition(semester));
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });

    }

    private void updateSubject(String title, String desc, String level, String sec_id, String semester, String school_year) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setTitle("Please wait");
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("subj_title", title);
        params.put("subj_id", subject_id);
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("subj_description", desc);
        params.put("subj_level", level);
        params.put("subj_semester", semester);
        params.put("subj_school_year", school_year);

        HttpProvider.postLogin(context, "controller_educator/update_subject_details.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    if (jsonArray.toString().contains("success")) {
                        Toasty.success(context, "Saved.").show();
//                        loadStudentsToSubject();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }
}
