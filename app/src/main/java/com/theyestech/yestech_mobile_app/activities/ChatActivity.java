package com.theyestech.yestech_mobile_app.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.ViewPagerAdapter;
import com.theyestech.yestech_mobile_app.fragments.CurrentChatFragment;
import com.theyestech.yestech_mobile_app.fragments.CurrentContactsFragment;
import com.theyestech.yestech_mobile_app.models.Chat;
import com.theyestech.yestech_mobile_app.models.Educator;
import com.theyestech.yestech_mobile_app.models.Student;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class ChatActivity extends AppCompatActivity {
    private ViewPagerAdapter viewPagerAdapter;
    private Context context;
    private ViewPager mViewPager;
    private ImageView iv_ChatBack;
    private String role;
    private ImageView profile_image;
    private TextView username;

    //Firebase
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        role = bundle.getString("ROLE");
        context = this;
    }

    @Override
    public void onStart() {
        super.onStart();
        //initializeUI();

        if (role.equals(UserType.Educator())) {
            getFirebaseEducator();

        } else {
            getFirebaseStudent();
        }
    }
//    private void initializeUI() {
//        iv_ChatBack = findViewById(R.id.iv_ChatBack);
//
//        iv_ChatBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        mViewPager = (ViewPager)findViewById(R.id.container1);
//        setupViewPager(mViewPager);
//
//        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(mViewPager);
//    }
//    private void setupViewPager(ViewPager viewPager){
//
//        Bundle bundle = new Bundle();
//        bundle.putString("ROLE", role);
//        CurrentChatFragment currentChatFragment = new CurrentChatFragment() ;
//        currentChatFragment.setArguments(bundle);
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPagerAdapter.addFragment(currentChatFragment, "Conversation");
//
//        CurrentContactsFragment currentContactsFragment = new CurrentContactsFragment();
//        currentContactsFragment.setArguments(bundle);
//        viewPagerAdapter.addFragment(currentContactsFragment, "Contacts");
//        viewPager.setAdapter(viewPagerAdapter);
//    }

    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Educator----------------------------------------//


    private void getFirebaseEducator() {
        final TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.container1);
        profile_image = findViewById(R.id.iv_ProfileEducatorImage);
        username = findViewById(R.id.username);
        iv_ChatBack = findViewById(R.id.iv_ChatBack);

        iv_ChatBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Educator user = dataSnapshot.getValue(Educator.class);
                assert user != null;
                username.setText(user.getUsername());
                username.setTextColor(Color.parseColor("#212121"));
                if (user.getImageURL().equals("default")){
                    Glide.with(context)
                            .load(R.drawable.ic_profile_accent)
                            .apply(GlideOptions.getGlideOptions())
                            .into(profile_image);
                } else {
                    Glide.with(context)
                            .load(user.getImageURL())
                            .apply(GlideOptions.getGlideOptions())
                            .into(profile_image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                int unread = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsseen()){
                        unread++;
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putString("ROLE", role);
                CurrentChatFragment currentChatFragment = new CurrentChatFragment() ;
                currentChatFragment.setArguments(bundle);
                if (unread == 0){
                    viewPagerAdapter.addFragment(currentChatFragment, "Chats");
                } else {
                    viewPagerAdapter.addFragment(currentChatFragment, "("+unread+") Chats");
                }
                CurrentContactsFragment currentContactsFragment = new CurrentContactsFragment();
                currentContactsFragment.setArguments(bundle);
                viewPagerAdapter.addFragment(currentContactsFragment, "Contacts");

                mViewPager.setAdapter(viewPagerAdapter);

                tabLayout.setupWithViewPager(mViewPager);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    //-----------------------------------------Firebase----------------------------------------//
    //-----------------------------------------Student----------------------------------------//


    private void getFirebaseStudent() {
        final TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.container1);
        profile_image = findViewById(R.id.iv_ProfileEducatorImage);
        username = findViewById(R.id.username);
        iv_ChatBack = findViewById(R.id.iv_ChatBack);

        iv_ChatBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Student").child(firebaseUser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Student student = dataSnapshot.getValue(Student.class);
                assert student != null;
                username.setText(student.getUsername());
                username.setTextColor(Color.parseColor("#212121"));
                if (student.getImageURL().equals("default")){
                    profile_image.setImageResource(R.mipmap.ic_launcher);
                } else {

                    //change this
                    Glide.with(getApplicationContext()).load(student.getImageURL()).into(profile_image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                int unread = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsseen()){
                        unread++;
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putString("ROLE", role);
                CurrentChatFragment currentChatFragment = new CurrentChatFragment() ;
                currentChatFragment.setArguments(bundle);
                if (unread == 0){
                    viewPagerAdapter.addFragment(currentChatFragment, "Chats");
                } else {
                    viewPagerAdapter.addFragment(currentChatFragment, "("+unread+") Chats");
                }
                CurrentContactsFragment currentContactsFragment = new CurrentContactsFragment();
                currentContactsFragment.setArguments(bundle);
                viewPagerAdapter.addFragment(currentContactsFragment, "Contacts");

                mViewPager.setAdapter(viewPagerAdapter);

                tabLayout.setupWithViewPager(mViewPager);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void statusEducator(String status){
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(firebaseUser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }

    private void statusStudent(String status){
        reference = FirebaseDatabase.getInstance().getReference("Student").child(firebaseUser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);

        reference.updateChildren(hashMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (role.equals(UserType.Educator())) {
            statusEducator("online");
        } else {
            statusStudent("online");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (role.equals(UserType.Educator())) {
            statusEducator("offline");
        } else {
            statusStudent("offline");
        }
    }
}
