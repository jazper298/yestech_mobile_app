package com.theyestech.yestech_mobile_app.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.theyestech.yestech_mobile_app.R;

public class MenuPrivacyAndPolicyActivity extends AppCompatActivity {

    private ImageView ivBack;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_and_policy);

        context = this;

        initializeUI();
    }

    private void initializeUI(){
        ivBack = findViewById(R.id.iv_MenuPrivacyAndPolicy);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
