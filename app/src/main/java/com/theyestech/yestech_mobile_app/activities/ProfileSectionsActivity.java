package com.theyestech.yestech_mobile_app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.SectionsAdapter;
import com.theyestech.yestech_mobile_app.adapters.StudentsAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Sections;
import com.theyestech.yestech_mobile_app.models.Students;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class ProfileSectionsActivity extends AppCompatActivity {

    private Context context;
    private ImageView ivBack, ivAdd;
    private EditText etSearch;
    private RecyclerView recyclerView;

    private SectionsAdapter sectionsAdapter;
    private Sections selected_section;
    private ArrayList<Sections> sectionsArrayList = new ArrayList<>();

    private StudentsAdapter studentsAdapter;
    private Students selected_student;
    private ArrayList<Students> studentsArrayList = new ArrayList<>();

    private ProgressDialog progressDialog;

    private Boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_sections);

        KeyboardHandler.hideKeyboard(ProfileSectionsActivity.this);

        context = this;

        initializeUI();
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.iv_ProfileSectionBack);
        ivAdd = findViewById(R.id.iv_ProfileEditSubjectAdd);
        etSearch = findViewById(R.id.et_ProfileSectionSearch);
        recyclerView = findViewById(R.id.rv_ProfileSection);

        getSections();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdit = false;
                openEditSection();
            }
        });
    }

    private void openEditSection() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_section, null);
        final EditText etName;
        final TextView tvHeader;
        final Button btnSave, btnCancel;

        etName = dialogView.findViewById(R.id.et_AddSectionName);
        tvHeader = dialogView.findViewById(R.id.tv_AddSectionHeader);
        btnSave = dialogView.findViewById(R.id.btn_AddSectionSave);
        btnCancel = dialogView.findViewById(R.id.btn_AddSectionCancel);

        if (isEdit) {
            etName.setText(selected_section.getSec_name());
            tvHeader.setText("Edit Section");
        }

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etName.getText().toString().isEmpty()) {
                    Toasty.warning(context, "Please input section name.").show();
                } else {
                    saveSection(etName.getText().toString(), b);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.hide();
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void openDeleteSection() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Delete")
                .setIcon(R.drawable.ic_delete_black)
                .setMessage("Are you sure you want to delete " + selected_section.getSec_name() + "?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSection();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();
    }

    private void deleteSection() {
        Toasty.success(context,"Deleted.").show();
    }

    private void getSections() {

        sectionsArrayList.clear();

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/get_sections.php", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressDialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        Debugger.logD("SECTIONS: " + jsonArray.toString());
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("section_id");
                            String name = jsonObject.getString("section_name");
                            String year = jsonObject.getString("section_year");

                            Sections sections = new Sections(id, name, year);

                            sectionsArrayList.add(sections);
                        }

                        recyclerView.setLayoutManager(new GridLayoutManager(ProfileSectionsActivity.this, 2));
                        recyclerView.setHasFixedSize(true);
                        sectionsAdapter = new SectionsAdapter(context, sectionsArrayList);
                        sectionsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selected_section = sectionsArrayList.get(position);
                                isEdit = true;

                                PopupMenu popup = new PopupMenu(context, view);
                                popup.inflate(R.menu.edit_delete_popup_menu);

                                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.popup_edit:
                                                openEditSection();
                                                return true;
                                            case R.id.popup_delete:
                                                openDeleteSection();
                                                return true;
                                        }
                                        return false;
                                    }
                                });
//                            openEditSection();
                                popup.show();
                            }
                        });
                        recyclerView.setAdapter(sectionsAdapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.dismiss();
                    Toasty.error(context, "Something went wrong, please check your internet connection.").show();

                }
        });
    }

    private void saveSection(String sec_name, final AlertDialog b) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("section_name", sec_name);

        if (isEdit) {
            HttpProvider.defaultPost(context, "controller_educator/edit_section.php", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressDialog.hide();
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));

                        Toasty.success(context, "Updated.").show();
                        b.hide();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD(e.toString());
                    }

                    getSections();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.hide();
                    Toasty.error(context, "Something went wrong, please check your internet connection.").show();

                }
            });
        } else {
            HttpProvider.defaultPost(context, "controller_educator/add_sections.php", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressDialog.hide();
                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));

                        Toasty.success(context, "Saved.").show();
                        b.hide();

                        getSections();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD(e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    progressDialog.hide();
                    Toasty.error(context, "Something went wrong, please check your internet connection.").show();

                }
            });
        }

    }

}
