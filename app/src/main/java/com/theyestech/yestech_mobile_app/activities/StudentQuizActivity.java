package com.theyestech.yestech_mobile_app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.helpers.QuizDbHelper;
import com.theyestech.yestech_mobile_app.models.QuestionEnumeration;
import com.theyestech.yestech_mobile_app.models.QuestionMultipleChoice;
import com.theyestech.yestech_mobile_app.models.QuestionTrueFalse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class StudentQuizActivity extends AppCompatActivity {
    public static final String EXTRA_SCORE = "extraScore";
    private static final long COUNTDOWN_IN_MILLIS = 30000;

    private static final String KEY_SCORE = "keyScore";
    private static final String KEY_QUESTION_COUNT = "keyQuestionCount";
    private static final String KEY_MILLIS_LEFT = "keyMillisLeft";
    private static final String KEY_ANSWERED = "keyAnswered";
    private static final String KEY_QUESTION_LIST = "keyQuestionList";

    private Context context;
    private EditText input_Answer;
    private TextView textViewQuestion;
    private TextView textViewScore;
    private TextView textViewQuestionCount;
    private TextView textViewCategory;
    private TextView textViewCountDown;
    private RadioGroup rbGroup;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private RadioButton rb4;
    private Button buttonConfirmNext;

    private ColorStateList textColorDefaultRb;
    private ColorStateList textColorDefaultCd;

    private CountDownTimer countDownTimer;
    private long timeLeftInMillis;

    private ArrayList<QuestionMultipleChoice> questionList;
    private ArrayList<QuestionTrueFalse> questionList1;
    private ArrayList<QuestionEnumeration> questionList2;
    private int questionCounter;
    private int questionCountTotal;
    private QuestionMultipleChoice currentQuestion;
    private QuestionTrueFalse currentQuestion1;
    private QuestionEnumeration currentQuestion2;

    private int score;
    private boolean answered;

    private long backPressedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        int categoryID = intent.getIntExtra(StartingQuizActivity.EXTRA_CATEGORY_ID, 0);
        String categoryName = intent.getStringExtra(StartingQuizActivity.EXTRA_CATEGORY_NAME);

        if (categoryName.equals("Multiple Choice")){
            setContentView(R.layout.activity_student_quiz);
            context = this;
            initializeUI();
            textViewCategory.setText("Category: " + categoryName);
            if (savedInstanceState == null) {
                QuizDbHelper dbHelper = QuizDbHelper.getInstance(this);
                questionList = dbHelper.getQuestions(categoryID);
                questionCountTotal = questionList.size();
                Collections.shuffle(questionList);

                showNextQuestion();
            } else {
                questionList = savedInstanceState.getParcelableArrayList(KEY_QUESTION_LIST);
                questionCountTotal = questionList.size();
                questionCounter = savedInstanceState.getInt(KEY_QUESTION_COUNT);
                currentQuestion = questionList.get(questionCounter - 1);
                score = savedInstanceState.getInt(KEY_SCORE);
                timeLeftInMillis = savedInstanceState.getLong(KEY_MILLIS_LEFT);
                answered = savedInstanceState.getBoolean(KEY_ANSWERED);

                if (!answered) {
                    startCountDown();
                } else {
                    updateCountDownText();
                    showSolution();
                }
            }
        }
        else if (categoryName.equals("True or False")){
            setContentView(R.layout.activity_student_true_or_false);
            context = this;
            initializeTrueFalseUI();
            textViewCategory.setText("Category: " + categoryName);
            if (savedInstanceState == null) {
                QuizDbHelper dbHelper = QuizDbHelper.getInstance(this);
                questionList1 = dbHelper.getTrueFalseQuestions(categoryID);
                questionCountTotal = questionList1.size();
                Collections.shuffle(questionList1);

                showNextTrueFalseQuestion();
            } else {
                questionList1 = savedInstanceState.getParcelableArrayList(KEY_QUESTION_LIST);
                questionCountTotal = questionList1.size();
                questionCounter = savedInstanceState.getInt(KEY_QUESTION_COUNT);
                currentQuestion1 = questionList1.get(questionCounter - 1);
                score = savedInstanceState.getInt(KEY_SCORE);
                timeLeftInMillis = savedInstanceState.getLong(KEY_MILLIS_LEFT);
                answered = savedInstanceState.getBoolean(KEY_ANSWERED);

                if (!answered) {
                    startCountDown();
                } else {
                    updateCountDownText();
                    showTrueFalseSolution();
                }
            }
        }
        else if (categoryName.equals("Enumeration")){
            setContentView(R.layout.activity_student_enumeration);
            context = this;
            initializeEnumUI();
            textViewCategory.setText("Category: " + categoryName);
            if (savedInstanceState == null) {
                QuizDbHelper dbHelper = QuizDbHelper.getInstance(this);
                questionList2 = dbHelper.getEnumerationQuestions(categoryID);
                questionCountTotal = questionList2.size();
                Collections.shuffle(questionList2);

                showNextEnumQuestion();
            } else {
                questionList2 = savedInstanceState.getParcelableArrayList(KEY_QUESTION_LIST);
                questionCountTotal = questionList2.size();
                questionCounter = savedInstanceState.getInt(KEY_QUESTION_COUNT);
                currentQuestion2 = questionList2.get(questionCounter - 1);
                score = savedInstanceState.getInt(KEY_SCORE);
                timeLeftInMillis = savedInstanceState.getLong(KEY_MILLIS_LEFT);
                answered = savedInstanceState.getBoolean(KEY_ANSWERED);

                if (!answered) {
                    startCountDown();
                } else {
                    updateCountDownText();
                    showSolution();
                }
            }
        }
        else {

        }


    }

    private void initializeUI() {
        textViewQuestion = findViewById(R.id.text_view_question);
        textViewScore = findViewById(R.id.tv_Score);
        textViewQuestionCount = findViewById(R.id.tv_Question_Count);
        textViewCategory = findViewById(R.id.tv_Category);
        textViewCountDown = findViewById(R.id.tv_Countdown);
        rbGroup = findViewById(R.id.radio_group);
        rb1 = findViewById(R.id.radio_button1);
        rb2 = findViewById(R.id.radio_button2);
        rb3 = findViewById(R.id.radio_button3);
        rb4 = findViewById(R.id.radio_button4);
        buttonConfirmNext = findViewById(R.id.button_confirm_next);

        textColorDefaultRb = rb1.getTextColors();
        textColorDefaultCd = textViewCountDown.getTextColors();

        buttonConfirmNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!answered) {
                    if (rb1.isChecked() || rb2.isChecked() || rb3.isChecked() || rb4.isChecked()) {
                        checkAnswer();
                    } else {
                        Toasty.warning(context, "Please select an answer").show();
                    }
                } else {
                    showNextQuestion();
                }
            }
        });
    }
    private void initializeTrueFalseUI() {
        textViewQuestion = findViewById(R.id.text_view_question);
        textViewScore = findViewById(R.id.tv_Score);
        textViewQuestionCount = findViewById(R.id.tv_Question_Count);
        textViewCategory = findViewById(R.id.tv_Category);
        textViewCountDown = findViewById(R.id.tv_Countdown);
        rbGroup = findViewById(R.id.radio_group);
        rb1 = findViewById(R.id.radio_button1);
        rb2 = findViewById(R.id.radio_button2);
        buttonConfirmNext = findViewById(R.id.button_confirm_next);

        textColorDefaultRb = rb1.getTextColors();
        textColorDefaultCd = textViewCountDown.getTextColors();

        buttonConfirmNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!answered) {
                    if (rb1.isChecked() || rb2.isChecked()) {
                        checkTrueFalseAnswer();
                    } else {
                        Toasty.warning(context, "Please select an answer").show();
                    }
                } else {
                    showNextTrueFalseQuestion();
                }
            }
        });
    }
    private void initializeEnumUI() {
        textViewQuestion = findViewById(R.id.text_view_question);
        textViewScore = findViewById(R.id.tv_Score);
        textViewQuestionCount = findViewById(R.id.tv_Question_Count);
        textViewCategory = findViewById(R.id.tv_Category);
        textViewCountDown = findViewById(R.id.tv_Countdown);
        input_Answer = findViewById(R.id.input_Answer);
        buttonConfirmNext = findViewById(R.id.button_confirm_next);

        textColorDefaultCd = textViewCountDown.getTextColors();

        buttonConfirmNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!answered) {
                    if (input_Answer.getText().length() == 0) {
                        Toasty.warning(context, "Please input your answer").show();
                    } else {
                        checkAnswer();
                    }
                } else {
                    showNextEnumQuestion();
                }
            }
        });
    }
    private void showNextQuestion() {
        rb1.setTextColor(textColorDefaultRb);
        rb2.setTextColor(textColorDefaultRb);
        rb3.setTextColor(textColorDefaultRb);
        rb4.setTextColor(textColorDefaultRb);
        rbGroup.clearCheck();

        if (questionCounter < questionCountTotal) {
            currentQuestion = questionList.get(questionCounter);

            textViewQuestion.setText(currentQuestion.getQuestion());
            rb1.setText(currentQuestion.getOption1());
            rb2.setText(currentQuestion.getOption2());
            rb3.setText(currentQuestion.getOption3());
            rb4.setText(currentQuestion.getOption4());

            questionCounter++;
            textViewQuestionCount.setText("Question: " + questionCounter + "/" + questionCountTotal);
            answered = false;
            buttonConfirmNext.setText("Confirm");

            timeLeftInMillis = COUNTDOWN_IN_MILLIS;
            startCountDown();
        } else {
            finishQuiz();
        }
    }
    private void showNextTrueFalseQuestion() {
        rb1.setTextColor(textColorDefaultRb);
        rb2.setTextColor(textColorDefaultRb);
        rbGroup.clearCheck();

        if (questionCounter < questionCountTotal) {
            currentQuestion1 = questionList1.get(questionCounter);

            textViewQuestion.setText(currentQuestion1.getQuestion());
            rb1.setText(currentQuestion1.getOption1());
            rb2.setText(currentQuestion1.getOption2());

            questionCounter++;
            textViewQuestionCount.setText("Question: " + questionCounter + "/" + questionCountTotal);
            answered = false;
            buttonConfirmNext.setText("Confirm");

            timeLeftInMillis = COUNTDOWN_IN_MILLIS;
            startCountDown();
        } else {
            finishQuiz();
        }
    }
    private void showNextEnumQuestion(){
        if (questionCounter < questionCountTotal) {
            currentQuestion2 = questionList2.get(questionCounter);

            textViewQuestion.setText(currentQuestion2.getQuestion());

            questionCounter++;
            textViewQuestionCount.setText("Question: " + questionCounter + "/" + questionCountTotal);
            answered = false;
            buttonConfirmNext.setText("Confirm");

            timeLeftInMillis = COUNTDOWN_IN_MILLIS;
            startCountDown();
        } else {
            finishQuiz();
        }
    }
    private void startCountDown() {
        countDownTimer = new CountDownTimer(timeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timeLeftInMillis = 0;
                updateCountDownText();
                checkAnswer();
            }
        }.start();
    }
    private void updateCountDownText() {
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        textViewCountDown.setText(timeFormatted);

        if (timeLeftInMillis < 10000) {
            textViewCountDown.setTextColor(Color.RED);
        } else {
            textViewCountDown.setTextColor(textColorDefaultCd);
        }
    }

    private void checkAnswer() {
        answered = true;

        countDownTimer.cancel();

        RadioButton rbSelected = findViewById(rbGroup.getCheckedRadioButtonId());
        int answerNr = rbGroup.indexOfChild(rbSelected) + 1;

        if (answerNr == currentQuestion.getAnswerNr()) {
            score++;
            textViewScore.setText("Score: " + score);
        }

        showSolution();
    }
    private void checkTrueFalseAnswer() {
        answered = true;

        countDownTimer.cancel();

        RadioButton rbSelected = findViewById(rbGroup.getCheckedRadioButtonId());
        int answerNr = rbGroup.indexOfChild(rbSelected) + 1;

        if (answerNr == currentQuestion1.getAnswerNr()) {
            score++;
            textViewScore.setText("Score: " + score);
        }
        showTrueFalseSolution();
    }
    private void checkEnumAnswer() {

    }

    private void showSolution() {
        rb1.setTextColor(Color.RED);
        rb2.setTextColor(Color.RED);
        rb3.setTextColor(Color.RED);
        rb4.setTextColor(Color.RED);

        switch (currentQuestion.getAnswerNr()) {
            case 1:
                rb1.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer A is correct");
                break;
            case 2:
                rb2.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer B is correct");
                break;
            case 3:
                rb3.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer C is correct");
                break;
            case 4:
                rb4.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer D is correct");
                break;
        }

        if (questionCounter < questionCountTotal) {
            buttonConfirmNext.setText("Next");
        } else {
            buttonConfirmNext.setText("Finish");
        }
    }
    private void showTrueFalseSolution() {
        rb1.setTextColor(Color.RED);
        rb2.setTextColor(Color.RED);

        switch (currentQuestion1.getAnswerNr()) {
            case 1:
                rb1.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer A is correct");
                break;
            case 2:
                rb2.setTextColor(Color.GREEN);
                textViewQuestion.setText("Answer B is correct");
                break;
        }

        if (questionCounter < questionCountTotal) {
            buttonConfirmNext.setText("Next");
        } else {
            buttonConfirmNext.setText("Finish");
        }
    }
    private void showEnumAnswer() {}
    private void finishQuiz() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_SCORE, score);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            finishQuiz();
        } else {
            Toasty.info(this, "Press back again to finish").show();
        }

        backPressedTime = System.currentTimeMillis();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_SCORE, score);
        outState.putInt(KEY_QUESTION_COUNT, questionCounter);
        outState.putLong(KEY_MILLIS_LEFT, timeLeftInMillis);
        outState.putBoolean(KEY_ANSWERED, answered);
        outState.putParcelableArrayList(KEY_QUESTION_LIST, questionList);
    }
}
