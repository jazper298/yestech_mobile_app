package com.theyestech.yestech_mobile_app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class UpdateProfileActivity extends AppCompatActivity {
    private Context context;
    //Widgets
    private Spinner sp_Gender;
    private EditText et_LastName, et_FirstName, et_MiddleName, et_Suffixes, et_Email, et_Password, et_Contact;
    private ImageView iv_Image;
    private Button btn_Update;

    private Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();

        KeyboardHandler.hideKeyboard(UpdateProfileActivity.this);
    }

    private void initializeUI(){
        sp_Gender = findViewById(R.id.sp_Gender);
        et_LastName = findViewById(R.id.et_LastName);
        et_FirstName = findViewById(R.id.et_FirstName);
        et_MiddleName = findViewById(R.id.et_MiddleName);
        et_Suffixes = findViewById(R.id.et_Suffixes);
        et_Email = findViewById(R.id.et_Email);
        et_Password = findViewById(R.id.et_Password);
        et_Contact = findViewById(R.id.et_Contact);
        iv_Image = findViewById(R.id.iv_Image);
        btn_Update = findViewById(R.id.btn_Update);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Gender.setAdapter(adapter);

        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserProfile();
                clearFields();
//                Toasty.success(getApplicationContext(), "Function is not ready yet").show();
            }
        });
    }

    //Wala pay API
    private void updateUserProfile() {
            final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

            RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("teach_lastname", et_LastName.getText().toString());
        params.put("teach_firstname", et_FirstName.getText().toString());
        params.put("teach_middlename", et_MiddleName.getText().toString());
        params.put("teach_suffixes", et_Suffixes.getText().toString());
        params.put("teach_email_address", et_Email.getText().toString());
            params.put("s_password", et_Password.getText().toString());
        params.put("teach_contact_number", et_Contact.getText().toString());
        params.put("teach_gender", sp_Gender.getSelectedItem().toString());

        HttpProvider.postLogin(context, "controller_educator/update_basic_details.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
//                    JSONObject jsonObject = jsonArray.getJSONObject(0);
//                    Debugger.logD(jsonObject.toString());
//                    if (jsonArray.toString().contains("success")) {
                    Toasty.success(context, "Saved.").show();
//                    loadProfileSubjects();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                    Toasty.success(context, "Saved.").show();
//                    loadProfileSubjects();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
//                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
//                loadProfileSubjects();
                Toasty.success(context, "Saved.").show();
            }
        });

    }
    private String imagetoString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void clearFields(){
        et_LastName.setText("");
        et_FirstName.setText("");
        et_MiddleName.setText("");
        et_Suffixes.setText("");
        et_Email.setText("");
        et_Password.setText("");
        et_Contact.setText("");
        sp_Gender.setSelection(-1);
        iv_Image.setImageResource(R.drawable.yes_logo);
    }
}
