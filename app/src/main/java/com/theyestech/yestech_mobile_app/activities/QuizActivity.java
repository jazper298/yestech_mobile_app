package com.theyestech.yestech_mobile_app.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.ViewPagerAdapter;
import com.theyestech.yestech_mobile_app.adapters.ViewPagerQuizAdapter;
import com.theyestech.yestech_mobile_app.fragments.CreateQuizFragment;
import com.theyestech.yestech_mobile_app.fragments.CurrentContactsFragment;

import es.dmoral.toasty.Toasty;

public class QuizActivity extends AppCompatActivity {


    private String quizId;
    private String quizTitle;
    private String quizType;
    private String quizItems;
    private TextView tv_QuizTitle;
    private TextView tv_QuizType;
    private TextView tv_QuizItems;
    private ViewPager view_pager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        quizId = bundle.getString("QUIZ_ID");

        initializeUI();
        showQuizType();
    }

    private void initializeUI() {
        tv_QuizTitle = findViewById(R.id.tv_QuizTitle);
        tv_QuizType = findViewById(R.id.tv_QuizType);
        tv_QuizItems = findViewById(R.id.tv_QuizItems);
        view_pager = findViewById(R.id.view_pager);

        tv_QuizTitle.setText("Quiz Title : " + quizTitle);
        tv_QuizType.setText("Quiz Type : " + quizType);
        tv_QuizItems.setText("Quiz Items : " + quizItems);
    }

    private void showQuizType() {
        Bundle bundle = new Bundle();
        bundle.putString("QuizTitle", quizTitle);
        bundle.putString("QuizType", quizType);
        bundle.putString("QuizItems", quizItems);

        ViewPagerQuizAdapter viewPagerAdapter = new ViewPagerQuizAdapter(getSupportFragmentManager());

        CreateQuizFragment createQuizFragment = new CreateQuizFragment();
        createQuizFragment.setArguments(bundle);
        viewPagerAdapter.addFragment(createQuizFragment);

        view_pager.setAdapter(viewPagerAdapter);
    }
}
