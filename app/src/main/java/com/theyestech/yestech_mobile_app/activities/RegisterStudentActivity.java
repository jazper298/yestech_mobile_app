package com.theyestech.yestech_mobile_app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.notifications.Data;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class RegisterStudentActivity extends AppCompatActivity {

    private Context context;
    private ImageView iv_AppLogo;
    private TextView tv_AppName;
    private TextView tv_StudentEducator;
    private Button btnNext;
    private View mProgressView;
    private View mLoginFormView;
    private EditText et_Username,et_Email, et_Password, et_ConfirmPassword;

    //Firebase
    FirebaseAuth auth;
    DatabaseReference reference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_student);

        context = this;
    }
    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();

        KeyboardHandler.hideKeyboard(RegisterStudentActivity.this);
    }
    private void initializeUI() {
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        iv_AppLogo = findViewById(R.id.iv_AppLogo);
        tv_AppName = findViewById(R.id.tv_AppName);
        btnNext = findViewById(R.id.btn_RegDone);
        et_Username = findViewById(R.id.et_Username);
        et_Email = findViewById(R.id.et_Email);
        et_Password = (EditText) findViewById(R.id.et_Password);
        et_ConfirmPassword = (EditText) findViewById(R.id.et_ConfirmPassword);

        auth = FirebaseAuth.getInstance();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                checkFields();
                if (fieldsAreEmpty()) {
                    Toasty.warning(context, "Please input all fields.").show();
                    et_Email.requestFocus();
                }
                else if (et_Password.getText().toString().equals( et_ConfirmPassword.getText().toString())){
                    registerStudent();
                }
                else {
                    Toasty.error(context, "Password didn't match!").show();
                }
            }
        });


    }
    private boolean fieldsAreEmpty() {
        if (et_Email.getText().toString().isEmpty() && et_Password.getText().toString().isEmpty()&& et_ConfirmPassword.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void registerStudent() {
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            RequestParams params = new RequestParams();
            params.put("s_email_address", et_Email.getText().toString());
            params.put("s_password", et_ConfirmPassword.getText().toString());

            StringEntity stringEntity = new StringEntity(params.toString());

            HttpProvider.post(context, "controller_student/register_as_student_class.php/", stringEntity, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
//                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
//                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Debugger.logD(responseBody.toString());

                    try {
                        String str = new String(responseBody, "UTF-8");
                        Debugger.logD(str);

                        firebaseRegister(et_Username.getText().toString(), et_Email.getText().toString(), et_ConfirmPassword.getText().toString(), progressDialog);



                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
//                    Intent main_intent = new Intent(getApplicationContext(), LoginActivity.class);
//                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(main_intent);
//                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toasty.error(context, "Error From Server 1").show();
                }

            });

        } catch (Exception err) {
            //Debugger.logD("FUCK " + err.toString());
            Toasty.error(context, err.toString()).show();
        }

    }

    //-----------------------------------------Firebase----------------------------------------//


//    private void checkFields() {
//        String txt_username = et_Username.getText().toString();
//        String txt_email = et_Email.getText().toString();
//        String txt_password = et_Password.getText().toString();
//        String txt_confirmpassword = et_ConfirmPassword.getText().toString();
//
//        if (TextUtils.isEmpty(txt_username) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password) || TextUtils.isEmpty(txt_confirmpassword)){
//            Toast.makeText(RegisterStudentActivity.this, "All fileds are required", Toast.LENGTH_SHORT).show();
//        } else if (txt_password.length() < 6 ){
//            Toast.makeText(RegisterStudentActivity.this, "password must be at least 6 characters", Toast.LENGTH_SHORT).show();
//        } else {
//            firebaseRegister(txt_username, txt_email, txt_confirmpassword);
//        }
//    }

    //Firebase Database
    private void firebaseRegister(final String username, String email, String password, final ProgressDialog progressDialog){
        final UserSessionEducator userSessionEducator = null;
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference("Student").child(userid);

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userid);
                            hashMap.put("username", username);
                            hashMap.put("imageURL", "default");
                            hashMap.put("status", "offline");
                            hashMap.put("search", username.toLowerCase());

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.hide();
                                    if (task.isSuccessful()){
                                        //openHomeFragment(role);
//                                        String firebaseUser = FirebaseAuth.getInstance().getCurrentUser().toString();
//                                        userSessionEducator.setFirebaseToken(firebaseUser);
                                        Intent intent = new Intent(RegisterStudentActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        Toasty.success(context, "Successfully Registered, Please check your email for verification.").show();
                                    }
                                }
                            });
                        } else {
                            progressDialog.hide();
                            Toasty.warning(context, "You can't register with this email or password").show();
                        }
                    }
                });
    }
}
