package com.theyestech.yestech_mobile_app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.adapters.ProfileSubjectsAdapter;
import com.theyestech.yestech_mobile_app.interfaces.OnClickRecyclerView;
import com.theyestech.yestech_mobile_app.models.Subjects;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static android.view.View.GONE;

public class ProfileSubjectsActivity extends AppCompatActivity {

    private Context context;

    //Widgets
    private RecyclerView rvSubjects, recyclerView2;
    private ImageView ivAdd, ivBack;

    private Subjects selected_subjects;

    private String role;

    private String selected_level, selected_semester, selected_section;
    private ArrayList<String> section_ids, section_names;

    private ProgressDialog progressDialog;

    private ArrayList<Subjects> subjectsArrayList = new ArrayList<>();
    private ProfileSubjectsAdapter profileSubjectsAdapter;

    private int numberOfColumns = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_subjects);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        role = bundle.getString("ROLE");

        KeyboardHandler.hideKeyboard(ProfileSubjectsActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        context = this;

        section_ids = new ArrayList<>();
        section_names = new ArrayList<>();

        initializeUI();
        loadProfileSubjects();
    }

    private void initializeUI() {
        rvSubjects = findViewById(R.id.rv_ProfileSubjects);
        ivAdd = findViewById(R.id.iv_ProfileSubjectsAdd);
        ivBack = findViewById(R.id.iv_ProfileSectionBack);

        if (role.equals("STUDENT")) {
            ivAdd.setVisibility(View.GONE);
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addForEducator();
            }
        });
    }

    private void addForEducator() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_subject, null);
        final EditText etTitle, etDescription;
        final Spinner spSection, spLevel, spSemester;
        final TextView tvSemester, tvSchoolYear;
        final Button btnSave;

        etTitle = dialogView.findViewById(R.id.subj_title);
        etDescription = dialogView.findViewById(R.id.subj_desc);
        spSection = dialogView.findViewById(R.id.sp_AddSubjectSection);
        spLevel = dialogView.findViewById(R.id.sp_AddSubjectLevel);
        spSemester = dialogView.findViewById(R.id.sp_AddSubjectSemester);
        tvSemester = dialogView.findViewById(R.id.tv_AddSubjectSemester);
        tvSchoolYear = dialogView.findViewById(R.id.tv_AddSubjectSchoolYear);
        btnSave = dialogView.findViewById(R.id.btn_AddSubjectSave);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.level, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLevel.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(context, R.array.semester, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSemester.setAdapter(adapter1);

        spSection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_section = section_ids.get(spSection.getSelectedItemPosition());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        getSections(params, spSection);

        spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    selected_level = "None";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                } else if (id == 1) {
                    selected_level = "Primary";
                    spSemester.setVisibility(GONE);
                    tvSemester.setVisibility(GONE);
                } else if (id == 2) {
                    selected_level = "Secondary";
                    spSemester.setVisibility(GONE);
                    tvSemester.setVisibility(GONE);
                } else if (id == 3) {
                    selected_level = "Tertiary";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    selected_semester = "";
                    spSemester.setVisibility(View.VISIBLE);
                    tvSemester.setVisibility(View.VISIBLE);
                } else if (id == 1) {
                    selected_semester = "1st";
                } else if (id == 2) {
                    selected_semester = "2nd";
                } else if (id == 3) {
                    selected_semester = "Summer";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        Calendar calendar = Calendar.getInstance();

        final String year = (calendar.get(Calendar.YEAR)) + " - " + (calendar.get(Calendar.YEAR) + 1);
        tvSchoolYear.setText(year);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = etTitle.getText().toString();
                String desc = etDescription.getText().toString();

                if (title.isEmpty()) {
                    Toasty.warning(context, "Please input subject title.").show();
                } else {
                    if (selected_level.equals("Primary") || selected_level.equals("Secondary")) {
                        saveSubject(title, desc, selected_level, selected_section, selected_semester, year);
                        b.hide();
                    } else if (selected_level.equals("Tertiary")) {
                        if (selected_semester.equals("")) {
                            Toasty.warning(context, "Please select semester.").show();
                        } else {
                            saveSubject(title, desc, selected_level, selected_section, selected_semester, year);
                            b.hide();
                        }
                    } else {
                        Toasty.warning(context, "Please select level.").show();
                    }
                }
            }
        });

        b.show();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void getSections(RequestParams params, final Spinner spSection) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        section_ids.clear();
        section_names.clear();

        HttpProvider.defaultPost(context, "controller_educator/get_sections.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.logD(jsonArray.toString());
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        section_ids.add(jsonObject.getString("section_id"));
                        section_names.add(jsonObject.getString("section_name"));
                    }

                    ArrayAdapter<String> sectionAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, section_names);
                    spSection.setAdapter(sectionAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });
    }

    private void saveSubject(String title, String desc, String level, String sec_id, String semester, String school_year) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("subj_title", title);
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));
        params.put("subj_description", desc);
        params.put("subj_level", level);
        params.put("section_id", sec_id);
        params.put("subj_semester", semester);
        params.put("subj_school_year", school_year);
        params.put("subj_file", "");

        HttpProvider.postLogin(context, "controller_educator/add_subjects.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
//                    JSONObject jsonObject = jsonArray.getJSONObject(0);
//                    Debugger.logD(jsonObject.toString());
//                    if (jsonArray.toString().contains("success")) {
                        Toasty.success(context, "Saved.").show();
                        loadProfileSubjects();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                    Toasty.success(context, "Saved.").show();
                    loadProfileSubjects();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
//                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
                loadProfileSubjects();
                Toasty.success(context, "Saved.").show();
            }
        });
    }


    private void loadProfileSubjects() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        subjectsArrayList.clear();

        RequestParams params = new RequestParams();
        params.put("teach_id", UserSessionEducator.getID(context));
        params.put("teach_token", UserSessionEducator.getToken(context));

        HttpProvider.defaultPost(context, "controller_educator/get_subjects.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String json = new String(responseBody);
                    JSONArray jsonArray = new JSONArray(new String(responseBody));
                    Debugger.printO("SUBJECTS: " + jsonArray);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String sub_id = jsonObject.getString("subj_id");
                        String sub_title = jsonObject.getString("subj_title");
                        String subj_file = jsonObject.getString("subj_file");

                        Subjects items = new Subjects();
                        items.setSubject_id(sub_id);
                        items.setSub_title(sub_title);
                        items.setSub_image(subj_file);

                        subjectsArrayList.add(items);
                    }

                    rvSubjects.setLayoutManager(new GridLayoutManager(ProfileSubjectsActivity.this, 2));
                    rvSubjects.setHasFixedSize(true);
                    profileSubjectsAdapter = new ProfileSubjectsAdapter(context, subjectsArrayList);
                    profileSubjectsAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selected_subjects = subjectsArrayList.get(position);

                            Intent intent = new Intent(context, SubjectDescActivity.class);
                            intent.putExtra("SUBJECT_ID", selected_subjects.getSubject_id());
                            startActivity(intent);
                        }
                    });

                    rvSubjects.setAdapter(profileSubjectsAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                Toasty.error(context, "Something went wrong, please check your internet connection.").show();
            }
        });

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        subjectsArrayList.clear();
//
//        loadProfileSubjects();
//    }
}
