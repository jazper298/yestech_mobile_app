package com.theyestech.yestech_mobile_app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.models.Educator;
import com.theyestech.yestech_mobile_app.models.Student;
import com.theyestech.yestech_mobile_app.utils.GlideOptions;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.theyestech.yestech_mobile_app.utils.UserSessionStudent;
import com.theyestech.yestech_mobile_app.utils.UserType;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {
    //Widgets
    private Context context;
    private TextView profile;
    private TextView tvEducatorFullname;
    private TextView username2;
    private TextView address;
    private ImageView ivEllipses;
    private ImageView profile_image, iv_ProfileBack, iv_ProfileBackground;
    private ImageView ivSubjects;
    private ImageView ivSections;
    private ImageView ivUpdate;
    private ImageView imageView8, imageView9, imageView10;

    private TextView tvEducatorViewAndEdit, tvEducatorPostsCount, tvEducatorSubjectsCount, tvEducatorSectionsCount;
    private ImageView ivEducatorImage, ivEducatorBack;
    private ConstraintLayout constraintEducatorSubjects, constraintEducatorSections, constraintEducatorVideos, constraintEducatorAssessment, constraintEducatorAnnouncements, constraintEducatorLogout;

    private Educator educator;
    private Student student;


    private String role;


    //Firebase
    DatabaseReference reference;
    FirebaseUser fuser;

    StorageReference storageReference;
    private static final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent extras = getIntent();
        Bundle bundle = extras.getExtras();
        role = bundle.getString("ROLE");

        if (role.equals(UserType.Educator())) {
            setContentView(R.layout.activity_profile_educator);
            context = this;
            initializeEducatorUI();
            setEducatorImage();

        } else {
            setContentView(R.layout.activity_profile_student);
            context = this;
            initializeStudentProfileUI();
            setStudentImage();
        }
    }

    //Student
    private void initializeStudentProfileUI() {
        profile_image = findViewById(R.id.profileImage);
        tvEducatorFullname = findViewById(R.id.profileName);
        ivEllipses = findViewById(R.id.iv_ProfileStudentBack);
        ivUpdate = findViewById(R.id.iv_UpdateProfile);
        imageView8 = findViewById(R.id.imageView8);
        imageView9 = findViewById(R.id.imageView9);
        imageView10 = findViewById(R.id.imageView10);
        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStudentImage();
            }
        });
        imageView8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileSubjectsActivity.class);
                intent.putExtra("ROLE", role);
                startActivity(intent);
            }
        });
        imageView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileSectionsActivity.class);
                startActivity(intent);
            }
        });
        ivEllipses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, v);
                popup.inflate(R.menu.menu_update_profile);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.update_profile:
                                //openAddQuiz();
                                Intent intent = new Intent(context, UpdateProfileActivity.class);
                                startActivity(intent);
                                return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
        //setStudentName();
    }

    //Educator
    private void initializeEducatorUI() {
        tvEducatorFullname = findViewById(R.id.tv_ProfileEducatorFullname);
        ivEducatorImage = findViewById(R.id.iv_ProfileEducatorImage);
        ivEducatorBack = findViewById(R.id.iv_ProfileEducatorBack);
        tvEducatorViewAndEdit = findViewById(R.id.tv_ProfileEducatorViewAndEdit);
        tvEducatorPostsCount = findViewById(R.id.tv_ProfileEducatorPostsCount);
        tvEducatorSubjectsCount = findViewById(R.id.tv_ProfileEducatorSubjectsCount);
        tvEducatorSectionsCount = findViewById(R.id.tv_ProfileEducatorSectionsCount);
        constraintEducatorSubjects = findViewById(R.id.constraint_ProfileEducatorSubjects);
        constraintEducatorSections = findViewById(R.id.constraint_ProfileEducatorSections);
        constraintEducatorVideos = findViewById(R.id.constraint_ProfileEducatorVideos);
        constraintEducatorAssessment = findViewById(R.id.constraint_ProfileEducatorAssessment);
        constraintEducatorAnnouncements = findViewById(R.id.constraint_ProfileEducatorAnnouncements);
        constraintEducatorLogout = findViewById(R.id.constraint_ProfileEducatorLogout);

        ivEducatorBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivEducatorImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEducatorImage();
            }
        });

        constraintEducatorSubjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileSubjectsActivity.class);
                intent.putExtra("ROLE", role);
                startActivity(intent);
            }
        });

        constraintEducatorSections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileSectionsActivity.class);
                startActivity(intent);
            }
        });

        tvEducatorViewAndEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateProfileActivity.class);
                startActivity(intent);
            }
        });

        constraintEducatorLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogLogout();
            }
        });
    }

    private void openDialogLogout() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Logout")
                .setIcon(R.drawable.ic_exit_logout)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logoutUser();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();

    }

    private void logoutUser() {
        UserSessionStudent.clearSession(context);
        UserType.clearRole(context);
        finish();

        //Firebase Logout
        FirebaseAuth.getInstance().signOut();
        // change this code beacuse your app will crash
        startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    //Educator
    private void setEducatorName() {
        String name = UserSessionEducator.getFirstname(context);

        if (name.equals(UserType.Educator())) {
            tvEducatorFullname.setText(educator.getUsername());
        } else {
            tvEducatorFullname.setText(educator.getUsername());
        }
    }

    private void setEducatorImage() {
        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Educator").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Educator user = dataSnapshot.getValue(Educator.class);

                if (UserSessionEducator.getFirstname(context).isEmpty() && UserSessionEducator.getLastname(context).isEmpty())
                    tvEducatorFullname.setText(UserSessionEducator.getEmail(context));
                else
                    tvEducatorFullname.setText(UserSessionEducator.getFirstname(context) + " " + UserSessionEducator.getLastname(context));

//                tvEducatorFullname.setText(user.getUsername());

                if (user.getImageURL().equals("default")) {
                    Glide.with(context)
                            .load(R.drawable.ic_profile_accent)
                            .apply(GlideOptions.getGlideOptions())
                            .into(ivEducatorImage);
                } else {

                    Glide.with(getApplicationContext())
                            .load(user.getImageURL())
                            .apply(GlideOptions.getGlideOptions())
                            .into(ivEducatorImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //Student
    private void setStudentName() {
        String name = UserSessionStudent.getFirstname(context);

        if (name.equals(UserType.Student())) {
            tvEducatorFullname.setText(student.getUsername());
        } else {
            tvEducatorFullname.setText(student.getUsername());
        }
    }

    private void openEducatorImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadEducatorImage() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Uploading . . .");
        progressDialog.show();

        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    + "." + getFileExtension(imageUri));

            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        String mUri = downloadUri.toString();

                        reference = FirebaseDatabase.getInstance().getReference("Educator").child(fuser.getUid());
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("imageURL", "" + mUri);
                        reference.updateChildren(map);

                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No image selected", Toast.LENGTH_SHORT).show();
        }
    }

    private void setStudentImage() {
        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Student").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Student student = dataSnapshot.getValue(Student.class);
                //
                tvEducatorFullname.setText(student.getUsername());
                if (student.getImageURL().equals("default")) {
                    profile_image.setImageResource(R.mipmap.ic_launcher);
                } else {
//                    Glide.with(getApplicationContext()).load(student.getImageURL()).into(profile_image);
                    RequestOptions myOption = new RequestOptions()
                            .centerInside()
                            .circleCrop();

                    Glide.with(getApplicationContext())
                            .load(student.getImageURL())
                            .apply(myOption)
                            .into(profile_image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void openStudentImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    private void uploadStudentImage() {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Uploading . . .");
        progressDialog.show();

        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    + "." + getFileExtension(imageUri));

            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        String mUri = downloadUri.toString();

                        reference = FirebaseDatabase.getInstance().getReference("Student").child(fuser.getUid());
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("imageURL", "" + mUri);
                        reference.updateChildren(map);

                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(context, "No image selected", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            imageUri = data.getData();

            if (uploadTask != null && uploadTask.isInProgress()) {
                Toast.makeText(context, "Upload in progress", Toast.LENGTH_SHORT).show();
            } else {
                if (role.equals(UserType.Educator())) {
                    uploadEducatorImage();
                } else {
                    uploadStudentImage();
                }

            }
        }
    }
}
