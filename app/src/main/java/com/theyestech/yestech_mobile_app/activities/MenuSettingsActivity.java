package com.theyestech.yestech_mobile_app.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.theyestech.yestech_mobile_app.R;

public class MenuSettingsActivity extends AppCompatActivity {
    private Context context;

    //Widgets
    private ImageView iv_MenuSettingsBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_settings);
    }
    @Override
    protected void onStart() {
        super.onStart();

        context = this;
        initializeUI();
    }

    private void initializeUI(){
        iv_MenuSettingsBack = findViewById(R.id.iv_MenuSettingsBack);
        iv_MenuSettingsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
