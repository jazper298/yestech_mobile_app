package com.theyestech.yestech_mobile_app.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theyestech.yestech_mobile_app.MainActivity;
import com.theyestech.yestech_mobile_app.R;
import com.theyestech.yestech_mobile_app.fragments.HomeFragment;
import com.theyestech.yestech_mobile_app.utils.Debugger;
import com.theyestech.yestech_mobile_app.utils.HttpProvider;
import com.theyestech.yestech_mobile_app.utils.KeyboardHandler;
import com.theyestech.yestech_mobile_app.utils.UserSessionEducator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class RegisterTeacherActivity extends AppCompatActivity {
    private Context context;
    private String role;
    private ImageView iv_AppLogo;
    private TextView tv_AppName;
    private TextView tv_StudentEducator;
    private Button btnNext;
    private View mProgressView;
    private View mLoginFormView;
    private EditText et_Username, et_Email, et_Password, et_ConfirmPassword;
    private FragmentManager fragmentManager;


    private ProgressDialog progressDialog;

    //Firebase
    FirebaseAuth auth;
    DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_teacher);
        context = this;
    }
    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();

        KeyboardHandler.hideKeyboard(RegisterTeacherActivity.this);
    }

    private void initializeUI() {


        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        iv_AppLogo = findViewById(R.id.iv_AppLogo);
        tv_AppName = findViewById(R.id.tv_AppName);
        btnNext = findViewById(R.id.btn_RegDone);

        //Firebase Database
        et_Username = findViewById(R.id.et_Username);
        et_Email = findViewById(R.id.et_Email);
        et_Password = (EditText) findViewById(R.id.et_Password);
        et_ConfirmPassword = (EditText) findViewById(R.id.et_ConfirmPassword);

        auth = FirebaseAuth.getInstance();


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                checkFields();
                if (fieldsAreEmpty()) {
                    Toasty.warning(context, "Please input all fields.").show();
                    et_Email.requestFocus();
                }
                else if (et_Password.getText().toString().equals( et_ConfirmPassword.getText().toString())){
                    registerEducator();
                    //checkFields();
                }
                else {
                    Toasty.error(context, "Password didn't match!").show();
                }

            }
        });


    }
    private boolean fieldsAreEmpty() {
        if (et_Email.getText().toString().isEmpty() && et_Password.getText().toString().isEmpty()&& et_ConfirmPassword.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void registerEducator() {
        final UserSessionEducator userSessionEducator = null;
        try {

            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            RequestParams params = new RequestParams();
            params.put("e_email_address", et_Email.getText().toString());
            params.put("e_password", et_ConfirmPassword.getText().toString());

            StringEntity stringEntity = new StringEntity(params.toString());

            HttpProvider.post(context, "controller_educator/register_as_educator_class.php/", stringEntity, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
//                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
//                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Debugger.logD(responseBody.toString());

                    try {
                        String str = new String(responseBody, "UTF-8");
                        Debugger.logD(str);
//                        UserSessionStudent session = new Gson().fromJson(responseBody.toString(), new TypeToken<UserSessionStudent>() {
//                        }.getType());
//                        session.saveUserSession(getApplicationContext());

                        firebaseRegister(et_Username.getText().toString(), et_Email.getText().toString(), et_ConfirmPassword.getText().toString(), progressDialog);



                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
//                    Intent main_intent = new Intent(getApplicationContext(), LoginActivity.class);
//                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(main_intent);
//                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toasty.error(context, "Error From Server 1").show();
                }

            });

        } catch (Exception err) {
            Debugger.logD("FUCK " + err.toString());
            Toasty.error(context, err.toString()).show();
        }

    }


    //-----------------------------------------Firebase----------------------------------------//


//    private void checkFields() {
//        String txt_username = et_Username.getText().toString();
//        String txt_email = et_Email.getText().toString();
//        String txt_password = et_Password.getText().toString();
//        String txt_confirmpassword = et_ConfirmPassword.getText().toString();
//
//        if (TextUtils.isEmpty(txt_username) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password) || TextUtils.isEmpty(txt_confirmpassword)){
//            Toast.makeText(RegisterTeacherActivity.this, "All fileds are required", Toast.LENGTH_SHORT).show();
//        } else if (txt_password.length() < 6 ){
//            Toast.makeText(RegisterTeacherActivity.this, "password must be at least 6 characters", Toast.LENGTH_SHORT).show();
//        } else {
//            firebaseRegister(txt_username, txt_email, txt_confirmpassword);
//        }
//    }

    //Firebase Database
    private void firebaseRegister(final String username, String email, String password, final ProgressDialog progressDialog){
        final UserSessionEducator userSessionEducator = null;
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference("Educator").child(userid);

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userid);
                            hashMap.put("username", username);
                            hashMap.put("imageURL", "default");
                            hashMap.put("status", "offline");
                            hashMap.put("search", username.toLowerCase());

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        progressDialog.hide();
                                        Intent intent = new Intent(RegisterTeacherActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        Toasty.success(context, "Successfully Registered, Please check your email for verification.").show();
                                    }
                                }
                            });
                        } else {
                            progressDialog.hide();
                            Toasty.warning(context, "You can't register with this email or password").show();
                        }
                    }
                });
    }

    private void openHomeFragment(String role) {
        setTitle("Home");
        Bundle bundle = new Bundle();
        bundle.putString("ROLE", role);
        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, homeFragment, homeFragment.getTag()).commit();
    }
}
