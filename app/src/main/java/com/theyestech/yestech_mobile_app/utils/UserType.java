package com.theyestech.yestech_mobile_app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserType {

    public static String Student() {
        return "STUDENT";
    }

    public static String Educator() {
        return "EDUCATOR";
    }

    private String userRole;

    public UserType(){

    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public static String getRole(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ROLE", "");
    }

    public boolean saveRole(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ROLE", userRole);
        return editor.commit();
    }

    public static boolean clearRole(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
