package com.theyestech.yestech_mobile_app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.annotations.SerializedName;

public class UserSessionStudent {

    @SerializedName("stud_id")
    private String studID;
    @SerializedName("stud_token")
    private String studToken;
    @SerializedName("stud_email_address")
    private String studEmail;
    @SerializedName("stud_firstname")
    private String studFirstname;
    @SerializedName("stud_lastname")
    private String studLastname;
    @SerializedName("stud_middlename")
    private String studMiddlename;
    @SerializedName("stud_suffixes")
    private String studSuffixes;
    @SerializedName("stud_gender")
    private String studGender;
    @SerializedName("stud_contact_number")
    private String studContactNumber;
    @SerializedName("stud_image")
    private String studImage;

    private String firebaseToken;

    public UserSessionStudent() {

    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getStudID() {
        return studID;
    }

    public void setStudID(String studID) {
        this.studID = studID;
    }

    public String getStudToken() {
        return studToken;
    }

    public void setStudToken(String studToken) {
        this.studToken = studToken;
    }

    public String getStudEmail() {
        return studEmail;
    }

    public void setStudEmail(String studEmail) {
        this.studEmail = studEmail;
    }

    public String getStudFirstname() {
        return studFirstname;
    }

    public void setStudFirstname(String studFirstname) {
        this.studFirstname = studFirstname;
    }

    public String getStudLastname() {
        return studLastname;
    }

    public void setStudLastname(String studLastname) {
        this.studLastname = studLastname;
    }

    public String getStudMiddlename() {
        return studMiddlename;
    }

    public void setStudMiddlename(String studMiddlename) {
        this.studMiddlename = studMiddlename;
    }

    public String getStudSuffixes() {
        return studSuffixes;
    }

    public void setStudSuffixes(String studSuffixes) {
        this.studSuffixes = studSuffixes;
    }

    public String getStudGender() {
        return studGender;
    }

    public void setStudGender(String studGender) {
        this.studGender = studGender;
    }

    public String getStudContactNumber() {
        return studContactNumber;
    }

    public void setStudContactNumber(String studContactNumber) {
        this.studContactNumber = studContactNumber;
    }

    public String getStudImage() {
        return studImage;
    }

    public void setStudImage(String studImage) {
        this.studImage = studImage;
    }

    public static String getID(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_ID", "");
    }

    public static String getToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_TOKEN", "");
    }

    public static String getEmail(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_EMAIL", "");
    }

    public static String getFirstname(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_FIRSTNAME", "");
    }

    public static String getLastname(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_LASTNAME", "");
    }

    public static String getMiddlename(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_MIDDLENAME", "");
    }

    public static String getSuffix(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_SUFFIX", "");
    }

    public static String getGender(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_GENDER", "");
    }

    public static String getContactNumber(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_CONTACTNUMBER", "");
    }

    public static String getImage(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("STUD_IMAGE", "");
    }

    public static String getFirebaseToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("FIREBASE_TOKEN", "");
    }

    public boolean saveUserSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("STUD_ID", studID);
        editor.putString("STUD_TOKEN", studToken);
        editor.putString("STUD_EMAIL",studEmail);
        editor.putString("STUD_FIRSTNAME",studFirstname);
        editor.putString("STUD_LASTNAME", studLastname);
        editor.putString("STUD_MIDDLENAME", studMiddlename);
        editor.putString("STUD_SUFFIX", studSuffixes);
        editor.putString("STUD_GENDER", studGender);
        editor.putString("STUD_CONTACTNUMBER", studContactNumber);
        editor.putString("STUD_IMAGE", studImage);
        editor.putString("FIREBASE_TOKEN", firebaseToken);
        return editor.commit();
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
