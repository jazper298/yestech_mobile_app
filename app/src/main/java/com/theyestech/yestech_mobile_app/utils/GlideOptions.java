package com.theyestech.yestech_mobile_app.utils;

import com.bumptech.glide.request.RequestOptions;

public class GlideOptions {

    public static RequestOptions getGlideOptions(){

        RequestOptions myOption = new RequestOptions();
        myOption.centerInside();
        myOption.circleCrop();

        return myOption;
    }
}
