package com.theyestech.yestech_mobile_app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.annotations.SerializedName;

public class UserSessionEducator {

    @SerializedName("user_id")
    private String teachID;
    @SerializedName("user_token")
    private String teachToken;
    @SerializedName("user_email_address")
    private String teachEmail;
    @SerializedName("user_firstname")
    private String teachFirstname;
    @SerializedName("user_lastname")
    private String teachLastname;
    @SerializedName("user_middlename")
    private String teachMiddlename;
    @SerializedName("user_suffixes")
    private String teachSuffixes;
    @SerializedName("user_gender")
    private String teachGender;
    @SerializedName("user_contact_number")
    private String teachContactNumber;
    @SerializedName("user_image")
    private String teachImage;

    private String firebaseToken;

    UserSessionEducator() {

    }

    public String getTeachID() {
        return teachID;
    }

    public void setTeachID(String teachID) {
        this.teachID = teachID;
    }

    public String getTeachToken() {
        return teachToken;
    }

    public void setTeachToken(String teachToken) {
        this.teachToken = teachToken;
    }

    public String getTeachEmail() {
        return teachEmail;
    }

    public void setTeachEmail(String teachEmail) {
        this.teachEmail = teachEmail;
    }

    public String getTeachFirstname() {
        return teachFirstname;
    }

    public void setTeachFirstname(String teachFirstname) {
        this.teachFirstname = teachFirstname;
    }

    public String getTeachLastname() {
        return teachLastname;
    }

    public void setTeachLastname(String teachLastname) {
        this.teachLastname = teachLastname;
    }

    public String getTeachMiddlename() {
        return teachMiddlename;
    }

    public void setTeachMiddlename(String teachMiddlename) {
        this.teachMiddlename = teachMiddlename;
    }

    public String getTeachSuffixes() {
        return teachSuffixes;
    }

    public void setTeachSuffixes(String teachSuffixes) {
        this.teachSuffixes = teachSuffixes;
    }

    public String getTeachGender() {
        return teachGender;
    }

    public void setTeachGender(String teachGender) {
        this.teachGender = teachGender;
    }

    public String getTeachContactNumber() {
        return teachContactNumber;
    }

    public void setTeachContactNumber(String teachContactNumber) {
        this.teachContactNumber = teachContactNumber;
    }

    public String getTeachImage() {
        return teachImage;
    }

    public void setTeachImage(String teachImage) {
        this.teachImage = teachImage;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public static String getID(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_ID", "");
    }

    public static String getToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_TOKEN", "");
    }

    public static String getEmail(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_EMAIL", "");
    }

    public static String getFirstname(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_FIRSTNAME", "");
    }

    public static String getLastname(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_LASTNAME", "");
    }

    public static String getMiddlename(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_MIDDLENAME", "");
    }

    public static String getSuffix(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_SUFFIX", "");
    }

    public static String getGender(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_GENDER", "");
    }

    public static String getContactNumber(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_CONTACTNUMBER", "");
    }

    public static String getImage(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TEACH_IMAGE", "");
    }

    public static String getFirebaseToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("FIREBASE_TOKEN", "");
    }

    public boolean saveUserSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("TEACH_ID", teachID);
        editor.putString("TEACH_TOKEN", teachToken);
        editor.putString("TEACH_EMAIL", teachEmail);
        editor.putString("TEACH_FIRSTNAME", teachFirstname);
        editor.putString("TEACH_LASTNAME", teachLastname);
        editor.putString("TEACH_MIDDLENAME", teachMiddlename);
        editor.putString("TEACH_SUFFIX", teachSuffixes);
        editor.putString("TEACH_GENDER", teachGender);
        editor.putString("TEACH_CONTACTNUMBER", teachContactNumber);
        editor.putString("TEACH_IMAGE", teachImage);
        editor.putString("FIREBASE_TOKEN", firebaseToken);
        return editor.commit();
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
